'''!@file               lab0x02_page.py

    @page page2         Lab0x02 Deliverables

    @section sec_intro  Abstract
                        The source code for this project can be found here: <a href="https://bitbucket.org/jarodlyles/me305_labs/src/master/Lab%200x02/">Lab0x02 Repository</a>.

                        The objective of this project was to use the Nucleo L476RG board to
                        interface with the Quadrature encoder of a DC motor.
                        In this lab, I was tasked to create a program that provide a user interface for user input to
                        query the position of the motor encoder (keyboard input 'P'), zero the motor encoder (keyboard input 'Z'), and collection motor encoder data over time for 30 seconds (keyboard input 'G').
                        Additionally, the motor encoder program provides instanteous position delta values when the spinning (keyboard input 'D').
                        Data collection can be suspended prematurely (keyboard input 'S').
                        In order to model this Finite-State Machine, I identified program operation with State Transition Diagrams and Task Diagrams.

                        \htmlonly
                        <img src="Lab0x02_Task_Diagram.png" alt="Lab0x02 Task Diagram.png" height="600">
                        \endhtmlonly

                        \htmlonly
                        <img src="Lab0x02_Task_User_STD" alt="Lab0x02 TU STD" height="600">
                        \endhtmlonly

                        \htmlonly
                        <img src="Lab0x02_Task_Encoder_STD.png" alt="Lab0x02 TE STD" height="600">
                        \endhtmlonly

                        From my state transition diagram, I coded the conditional logic
                        around tasks – a user task module and an encoder task module. The modules utilized shared boolean flags and numerical variables
                        to communicate state transitions and positional data. The communication between these tasks effectively accepted user input and
                        implemented an encoder class object class to retrieve desired encoder data.
                        The repository for the source code can be found at:
                        <a href="https://bitbucket.org/jarodlyles/me305_labs/src/master/Lab%200x02/">Lab0x02 Repository</a>.

                        Once programmed, I tested the functionality on the Nucleo L476RG.
                        As seen in the video below, the program intiates, accepts user inputs, and responds appropriately with relevant data.

                        \htmlonly
                        <iframe width="1280" height="745" src="https://youtu.be/MZ40IM8s2t8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly

                        After recording the motor encoder data over a 30 second period, I copied and saved the data to a delimited text file for processing. The plot below
                        demonstrates the motor encoder readings for a randomized, alternating human input to the shaft over time.

                        \htmlonly
                        <img src="Lab0x02_Encoder_Position_Plot.jpg.jpg" alt="Encoder Position Plot" height="600">
                        \endhtmlonly

    @author              Jarod Lyles

    @date                February 2, 2022
'''
