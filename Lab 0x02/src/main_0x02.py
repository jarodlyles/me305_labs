'''!@file       main_0x02.py
    @brief      A testing program for reading and recording Quadrature encoder 
                position data.
    @details    The main file executes the necessary tasks to carry out the program
                assigns necessary shared variables amongst source code files.
    @author     Jarod Lyles
    @date       February 3, 2022
'''

import shares_0x02
import task_user_0x02, task_encoder_0x02

## Assigns a shared boolean flag to manage zeroing the encoder
zFlag = shares_0x02.Share(False)
## Assigns a shared boolean flag to manage getting encoder position
pFlag = shares_0x02.Share(False)
## Assigns a shared boolean flag to manage getting the encoder position delta
dFlag = shares_0x02.Share(False)
## Assigns a shared boolean flag to manage collecting encoder data
gFlag = shares_0x02.Share(False)

## Assigns a shared position variable for handling of encoder position
position = shares_0x02.Share(0)
## Assigns a shared position variable for handling of encoder position delta
delta = shares_0x02.Share(0)

## Assigns a shared variable for collection of encoder position data
posData = shares_0x02.Share(0)
## Assigns a shared variable for collection of encoder time data
timeData = shares_0x02.Share(0)

## Defines the period of operation of the task functions in microseconds
period = 10_000
    
if __name__ == '__main__':
    
    ## Initializes the user task
    task1 = task_user_0x02.taskUser('taskUser', period, position, delta, zFlag, pFlag, dFlag, gFlag, posData, timeData)
    ## Intializes the encoder task
    task2 = task_encoder_0x02.taskEncoder('taskEncoder', period, position, delta, zFlag, pFlag, dFlag, gFlag, posData, timeData)
    taskList = [task1, task2]
    
    while True:
        try:
            for task in taskList:
                next(task)
        
        except KeyboardInterrupt:
            break
        
        
print('PROGRAM TERMINATING')
    
