'''!@file                   Lab0x01_Hardware.py
    @brief                  This file blinks an LED at the selection of a square, since, or sawtooth waveform.
    @details                This file, Lab0x01_Hardware, cycles through waveform profiles – sine, square, and sawtooth – that blink an on-board LED (LED2) on the Nucleo L476, changing waveforms according to an on-board button push from the user (B1).
    @author                 Jarod Lyles
    @author                 Simon Way
    @date                   January 18, 2022
'''

import time
import math
import pyb

## Initializes pin A5 connected to the on-board LED2 as output using the Pyboard
#  plug-in
pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
## Initializes a timer object using timer 2 at a frequency of 20,000 Hz
tim2 = pyb.Timer(2, freq = 20000)
## Initializes a channel object using channel 1 with a PWM signal input into 
#  pin A5 (LED2)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
## Initializes pin 13 connected to the on-board button B1
pinC13 = pyb.Pin(pyb.Pin.cpu.C13)

def onButtonPressCallback(IRQ_src):
    '''!@brief          Sets the button pressed to true if the button is pressed.
        @details        his function looks for the falling edge of a button
                        interrupt and set the global boolean value of the button
                        condition to true.
        @param IRQ_src  The falling-edge signal of the on-board button push.
    '''
    ## Defines a global boolean flag to be used for conditional processing 
    #  of the button press. The flag is boolean true when activated.
    global buttonPressed
    buttonPressed = True

def squareWave(t):
    '''!@brief          Defines a square wave as a function of time.
        @details        This function accepts a time value in seconds and uses
                        the modulus operator and the rounding function  to 
                        define a square waveform of period 1 second and amplitude 
                        1.
        @param t        The elapsed time since initiation of the waveform [seconds].
        @return         The ampltiude value of the square wave signal at the 
                        given time, t.
    '''
    return 1 - round(t%1)

def sineWave(t):
    '''!@brief          Defines a sine wave as a function of time.
        @details        This function accepts a time value in seconds and uses
                        the sine function of the math library to define a
                        wsine aveform of period 10 seconds and amplitude of 0.5
                        oscillating about a value of 0.5.
                        
        @param t        The elapsed time since initiation of the waveform [seconds].
        @return         The ampltiude value of the sine wave signal at the 
                        given time, t.
    '''
    return 0.5 + (0.5 * math.sin(2 * math.pi * t / 10))

def sawWave(t):
    '''!@brief          Defines a square wave as a function of time.
        @details        This function accepts a time value in seconds and uses
                        the modulus operator to define a sawtooth waveform of 
                        period 1 second and amplitude 1.
        @param t        The elapsed time since initiation of the waveform [seconds].
        @return         The ampltiude value of the square wave signal at the 
                        given time, t.
    '''
    return t%1

## Initializes an interrupt upon external input of pushing the on-board 
#  connected to pin 13 (Button B1)
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, 
                                   pull=pyb.Pin.PULL_NONE, 
                                   callback=onButtonPressCallback)

if __name__ == '__main__':
    buttonPressed = False
    ## Intializes a state variable of integer value to count the state of the 
    #  selected waveform
    state = 0
    print ('This program cycles through square, sine, and sawtooth LED profiles. \n To begin, please press the blue user interface button (B1) on the Nucleo')
    
    while True:
        try:
            
            if state == 0:
                #run state zero
                if buttonPressed:
                    print('Square Waveform selected')
                    buttonPressed = False
                    ## Initializes a long value of the start time of the waveform
                    startTime = 0
                    state = 1
                    
            elif state == 1:
                #run square wave
                
                ## Initializes a long value of the current time of the waveform
                currentTime = time.ticks_ms()
                ## Initializes a long value of the elasped wave time of the 
                #  waveform as a difference of the start time and the current time 
                #  [seconds]
                waveTime = time.ticks_diff(currentTime, startTime) / 1000
                
                ## Defines a brightness percentage value using the respective 
                #  waveform function to determine the PWM signal
                brightness = 100 * squareWave(waveTime)
                t2ch1.pulse_width_percent(brightness)
                
                if buttonPressed:
                    print('Sine Waveform selected')
                    buttonPressed = False
                    startTime = currentTime
                    state = 2
                    
            elif state == 2:
                #run sine wave
                
                currentTime = time.ticks_ms()
                waveTime = time.ticks_diff(currentTime, startTime) / 1000
                brightness = 100 * sineWave(waveTime)
                t2ch1.pulse_width_percent(brightness)
                
                if buttonPressed:
                    print('Sawtooth Waveform selected')
                    buttonPressed = False
                    startTime = currentTime
                    state = 3
            
            elif state == 3:
                # run saw wave
                
                currentTime = time.ticks_ms()
                waveTime = time.ticks_diff(currentTime, startTime) / 1000
                brightness = 100 * sawWave(waveTime)
                t2ch1.pulse_width_percent(brightness)

                if buttonPressed:
                    print('Square Waveform selected')
                    buttonPressed = False
                    startTime = currentTime
                    state = 1
            
        except KeyboardInterrupt:
            break
        
    print('ENDING OPERATIONS')
