'''!@file                lab0x01_page.py

    @page page1          Lab0x01 Deliverables
    
    @section sec_intro  Abstract
                        The source code for this project can be found here: <a href="bitbucket.org/jarodlyles/me305_labs/src/master/Lab%200x01/">Lab0x01 Repository</a>.
                        
                        The objective of this project was to become familiar 
                        with the Nucleo L476RG board and how to interface with it. 
                        In this lab, I was tasked to create a program that a user 
                        could select a LED brightness profile on the on-board LED2 
                        of the Nucleo. The user selects from a square, sine, 
                        and sawtooth waveform for the LED pulse profile using the on-board, 
                        blue input button on the Nucleo by pressing once to alternate 
                        between waveforms. In order to model this Finite-State Machine, 
                        I identified program operation with a State Transition Diagram, as seen below.
                        
                        \htmlonly
                        <img src="Lab0x01_STD.png" alt="Lab0x01 STD" height="600">
                        \endhtmlonly
                        
                        From my state transition diagram, I coded the conditional logic
                        around time-based functions that would be needed to operate
                        the program. The repository for the source code can be found at:
                        <a href="bitbucket.org/jarodlyles/me305_labs/src/master/Lab%200x01/">Lab0x01 Repository</a>.
                        
                        Once programmed, I tested the functionality on the Nucleo L476RG.
                        As seen in the video below, the program intiates, waits for
                        button interrupt from the user, prints the selected waveform
                        in the REPL upon button push, and successfully alternates
                        through the LED waveform profiles.
                        
                        \htmlonly
                        <iframe width="1280" height="745" src="https://www.youtube.com/embed/aDDfpMSnXuQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly

    @author              Jarod Lyles

    @date                January 19, 2022
'''
