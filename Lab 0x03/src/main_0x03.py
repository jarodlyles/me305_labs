'''!@file       main_0x03.py
    @brief      A testing program for reading and recording Quadrature encoder 
                position data.
    @details    The main file executes the necessary tasks to carry out the program
                assigns necessary shared variables amongst source code files.
    @author     Jarod Lyles
    @date       February 3, 2022
'''

import shares_0x03
import task_user_0x03, task_encoder_0x03, task_motor_0x03

## Assigns a shared boolean flag to manage zeroing the encoder
zFlag = shares_0x03.Share(False)
## Assigns a shared boolean flag to manage getting encoder position
pFlag = shares_0x03.Share(False)
## Assigns a shared boolean flag to manage getting the encoder position delta
dFlag = shares_0x03.Share(False)
## Assigns a shared boolean flag to manage collecting encoder data
gFlag = shares_0x03.Share(False)
## Assigns a shared boolean flag to manage setting duty cycle
mFlag = shares_0x03.Share(False)
## Assigns a shared boolean flag to manage setting motor fault conditions
cFlag = shares_0x03.Share(False)
## Assigns a shared boolean flag to manage 
vFlag = shares_0x03.Share(False)
## Assigns a shared boolean flag to manage testing interface
tFlag = shares_0x03.Share(False)

## Assigns a shared position variable for handling of encoder position
position = shares_0x03.Share(0)
## Assigns a shared position variable for handling of encoder position delta
delta = shares_0x03.Share(0)
## Assigns a shared velocity varibale for handling of encoder read velocity
velocity = shares_0x03.Share(0)

## Assigns a shared variable for collection of encoder position data
posData = shares_0x03.Share(0)
## Assigns a shared variable for collection of encoder time data
timeData = shares_0x03.Share(0)
## Assigns a shared variable for collection of encoder velocity data
velData = shares_0x03.Share(0)

## Assigns a shared variable for motor 1 duty cycle
duty1 = shares_0x03.Share(0)
## Assigns a shared variable for motor 2 duty cycle
duty2 = shares_0x03.Share(0)

## Defines the period of operation of the task functions in microseconds
period = 10_000
    
if __name__ == '__main__':
    
    ## Initializes the user task
    task1 = task_user_0x03.taskUser('taskUser', period, position, delta, velocity, zFlag, pFlag, dFlag, gFlag, mFlag, cFlag, vFlag, tFlag, velData, posData, timeData, duty1, duty2)
    ## Intializes the encoder task
    task2 = task_encoder_0x03.taskEncoder('taskEncoder', period, position, delta, velocity, zFlag, pFlag, dFlag, gFlag, vFlag, velData, posData, timeData)
    ## Initializes the motor task
    task3 = task_motor_0x03.taskMotor('taskMotor', period, cFlag, duty1, duty2)
    
    taskList = [task1, task2, task3]
    
    while True:
        try:
            for task in taskList:
                next(task)
        
        except KeyboardInterrupt:
            break
        
        
print('PROGRAM TERMINATING')
    
