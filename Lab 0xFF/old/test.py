

import numpy as np
t = np.empty((9,3))
print(t)

calibrationPoints = np.array([[-88, 50], [88, 50], [88, -50], [-88, -50], [0, 0]])
print(calibrationPoints)

x = (0, 0)
y = (1, 1)
x_d = (2, 2)
y_d = (3, 3)

T = np.array([x, y, x_d, y_d])
print(T)
print(T.T)

K = np.array([1, 1, 1, 1])

F = np.dot(T.T, K)
print(F)
F = np.dot(K, T)
print(F)
const = 2
out = const * F
print(out)