'''!@file       ClosedLoop.py
    @brief      A driver for defining a closed loop object
    @details    This file consitutes a class that defines a closed loop motor calculation.
                This object accepts positional, valocity, and initial values to determine
                closed-loop motor control.
    @author     Jarod Lyles
    @date       March 5th, 2022
'''
from ulab import numpy as np
#import numpy as np
class ClosedLoop:
    '''!@brief      Constructs an closed loop object.
        @details    This method initializes variables of an object, namely the adjusted duty cycle value.             
    '''
    def __init__(self, eulerAngle, gyrVel, ballVel, ballPos):
        '''!@brief      Constructs an closed loop object.
            @details    This method initializes variables of an object, namely the adjusted duty cycle value.             
        '''
        ## Initializes an attribute for the adjust duty cycle calculation.
        self.L = (0, 0)
        ## Assigns an attribute for the positional theta values
        self.theta = eulerAngle
        ## Assigns an attribute for the angular velocity values
        self.theta_dot = gyrVel
        ## Assigns an attribute for the positional theta set point value
        self.theta_ref = 0
        ## Assigns an attribute for the initial angular velocity values
        self.theta_dot_ref = 0
        self.x = ballPos
        self.x_dot = ballVel

        self.R = 2.21   # ohms
        self.V_dc = 12  # V
        self.K_v = 13.9 # N-m/A
        self.const = ((self.R * 1_000) / (self.K_v * self.V_dc))
        self.K = np.array([ [1.2], [5], [500], [0.2] ])
        


    def update(self):
        '''!@brief      Performs update of the closed loop system.
            @details    This method performs update of the closed loop system, accepting
                        positional, velocity, and set point data to do calculation.             
        '''
        
        #self.error = np.array([self.x, self.x_dot, self.theta, self.theta_dot])
        #self.T = np.dot(self.error.T, -self.K)
        # Calculates the closed-loop adjusted duty cycle
        self.L = self.const * self.T
        self.L = (self.K_p * (self.theta_ref - self.theta)) + (self.K_d * (self.theta_dot_ref - self.theta_dot))
        
        if self.L > 45:
            self.L = 45
        elif self.L < -45:
            self.L = -45
        else:
            self.L = self.L[0][0]
        '''
        if self.L[1][0] > 45:
            self.L[1][0] = 45
        elif self.L[1][0] < -45:
            self.L[1][0] = -45
        else:
            self.L[1][0] = self.L[1][0]
        '''
        return self.L

    def set_gain(self, K_p, K_d, K_i):
        '''!@brief      Sets the closed-loop gains of the system.
            @details    This method assigns new values to the proportional, derivative, and integral
                        gain control constants of the system.             
        '''
        ## Assigns an attribute for the proportional gain
        self.K_p = K_p
        ## Assigns an attribute for the derivative gain
        self.K_d = K_d
        ## Assigns an attribute for the integral gain
        self.K_i = K_i
    
    def get_gain(self):
        '''!@brief      Returns the value of the closed loop gain variables.
            @details    This method returns values of the proportional, derivative, and integral
                        gain control constants.             
        '''
        return (self.K_p, self.K_d, self.K_i)

if __name__ == "__main__":
    # test
    CL = ClosedLoop(6, 7, 8, 9)
    print(CL.update())

