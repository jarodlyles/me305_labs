'''!@file       motor_0xFF.py
    @brief      A driver for motor control using a duty cycle input.
    @details    This class defines a motor object that can be controlled with a duty cycle (PWM signal).
    @author     Jarod Lyles
    @date       February 17, 2022
'''

from pyb import Pin, Timer
import time

class Motor: 
    '''!@brief      A motor class for one channel.
        @details    Class can be used to apply PWM to a
                    DC motor.
    '''
    def __init__(self, freq, outA, IN_A_pin, outB, IN_B_pin): 
        '''!@brief          Initializes an object associated with a DC Motor.
            @details         Use the DRV8847 class to instantiate a motor object of this class.
            @param freq      Specifies frequency of the PWM timer.
            @param IN_A_pin  Specifies first pin to use with specific motor.
            @param IN_B_pin  Specifies second pin to use with specific motor.
            @param outA      Channel of the first pin used for motor control.
            @param outB      Channel of the second pin used for motor control.
        '''
        # Timer initialization
        self.PWM_tim = Timer(3, freq = freq) 
        
        # Pin initialization 
        self.IN_A = Pin(IN_A_pin, mode=Pin.OUT_PP) 
        self.IN_B = Pin(IN_B_pin, mode=Pin.OUT_PP) 
        # Channel initialization
        self.t3chA = self.PWM_tim.channel(outA, Timer.PWM_INVERTED, pin=self.IN_A) 
        self.t3chB = self.PWM_tim.channel(outB, Timer.PWM_INVERTED, pin=self.IN_B) 
          
     
    def set_duty(self, duty):
        '''!@brief          Set the PWM duty cycle for the motor channel.
            @details        This method set the PWM duty cycle of the given motor. Direction of rotation is determined by sign on the input value.
            @param duty     A signed number of the duty cycle in the range of -100% to 100%                          
        '''

        # For a positive duty cycle
        if duty > 0: 
            # Drive channel A, channel B off
            self.t3chA.pulse_width_percent(abs(duty))
            self.t3chB.pulse_width_percent(0)

        # For a negative duty cycle
        elif duty < 0: 
            # Drive channel B, channel A off
            self.t3chA.pulse_width_percent(0)
            self.t3chB.pulse_width_percent(abs(duty))
