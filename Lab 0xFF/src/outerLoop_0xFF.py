'''!@file       outerLoop_0xFF.py
    @brief      A driver for defining the outer closed-loop object of the ball balancing control system.
    @details    This file constitutes a class that defines a closed loop motor calculation.
                This object accepts ball positional, velocity, and initial values to determine
                closed-loop motor control in the form of an reference platform angle response. This
                calculation feeds into the set point of the inner closed-loop system.
    @author     Jarod Lyles
    @date       March 17th, 2022
'''
class OuterLoop:

    def __init__(self):
        '''!@brief      Constructs an closed loop object.
            @details    This method initializes variables of an object, namely the adjusted duty cycle value.             
        '''
        ## Initializes an attribute for the adjust platform reference angle.
        self.theta = 0
        ## Initializes an attribute for the outer loop proportional control gain.
        self.K_p = 0.09
        ## Initializes an attribute for the outer loop derivative control gain.
        self.K_d = -0.03

    def update(self, ballPos, ballVel):
        '''!@brief      Performs update of the closed loop system.
            @details    This method performs update of the closed loop system, accepting
                        positional, velocity, and set point data to do calculation.      
            @return     The reference angle of the platform       
        '''
        ## Assigns an attribute for the positional theta values
        self.x = ballPos
        ## Assigns an attribute for the angular velocity values
        self.x_dot = ballVel
        ## Assigns an attribute for the positional theta set point value
        self.x_ref = 0
        ## Assigns an attribute for the initial angular velocity values
        self.x_dot_ref = 0
        
        # Calclates the closed-loop adjusted duty cycle
        self.theta = (self.K_p * (self.x_ref - self.x)) + (self.K_d * (self.x_dot_ref - self.x_dot)) + self.comp
        
        # Saturation limit
        if self.theta > 12:
            self.theta = 12
        elif self.theta < -12:
            self.theta = -12
        else: 
            self.theta = self.theta
        
        #print(f'x {self.x}    x_dot {self.x_dot}    theta {self.theta}')
        return self.theta

    def set_gain(self, K_p, K_d, K_i, comp):
        '''!@brief      Sets the outer closed-loop gains of the system.
            @details    This method assigns new values to the proportional, derivative, and integral
                        gain control constants of the system.             
        '''
        ## Assigns an attribute for the proportional gain
        self.K_p = K_p
        ## Assigns an attribute for the derivative gain
        self.K_d = K_d
        ## Assigns an attribute for the integral gain
        self.K_i = K_i
        ## Assigns an attribute for angle compensation
        self.comp = comp
    
    def get_gain(self):
        '''!@brief      Returns the value of the outer closed-loop gain variables.
            @details    This method returns values of the proportional, derivative, and integral
                        gain control constants.             
        '''
        return (self.K_p, self.K_d, self.K_i)

