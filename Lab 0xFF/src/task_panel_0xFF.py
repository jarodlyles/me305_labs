'''!@file       task_panel_0xFF.py
    @brief      A module task for operating the BNO055 driver.
    @details    This file contains a task for properly managing the BNO055 driver.
                The task checks the status of the calibration, calibrates if needed, and 
                returns Euler angles and angular velocities. Alternatively, if calibration
                is not needed, calibration coefficients are pulled from an existing calibration
                constants file.
    @author     Jarod Lyles
    @date       March 5, 2022
'''

import pyb, time, panel_0xFF, os
from ulab import numpy as np
from micropython import const

## Defines a constant for State 0 of IMU task
S0_INIT = const(0)
## Defines a constant for State 1 of IMU task
S1_UPDATE_PLATFORM = const(1)
## Defines a constant for State 2 of IMU task
S2_CALIBRATE = const(2)
## Defines a constant for State 3 of IMU task
S3_SAVE_CALIBRATION = const(3)
## Defines a constant for State 4 of IMU task
S4_WRITE_COEFFICIENTS = const(4)

'''
CAL_POS_1 = const(1)
CAL_POS_2 = const(2)
CAL_POS_3 = const(3)
CAL_POS_4 = const(4)
CAL_POS_5 = const(5)
'''
## Initializes a NumPy array of the calibration locations on the touch panel
calibrationPoints = np.array([[0, 0], [-80, 40], [80, 40], [80, -40], [-80, -40]])

## Initializes a BNO055 IMU object
PANEL = panel_0xFF.TouchPanel()

def taskPanel(taskName, period, ballPos, ballVel):
    '''!@brief           The module tasks the touch panel according to state conditions.
        @details         This task accepts shared variables of positional and velocity data
                         and tasks the 
                         shared Euler angles and gyroscope velocities.
                         appropreiate state transitions and return values to
                         to the ueser interface.
        @param taskName  Assigns a name to the taskUser task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param ballPos   Provides the shared variable for ball position with values for x- and y- position
        @param ballVel   Provides the shared variable for ball velocity with values for x- and y- velocities
                         
    '''
    
    ## Initializes the start time of the task
    start_time = time.ticks_us()
    ## Initializes the next time of the task
    next_time = time.ticks_add(start_time, period)
    ## Defines the file name to which touch panel coefficients exist / are written
    file_name = "Panel_cal_coeffs.txt"
    ## Defines a variable for the state of the system
    state = S0_INIT
    ## Initializes a variable for current x-position of the ball
    x_k = 0
    ## Initializes a variable for current y-position of the ball
    y_k = 0
    ## Initializes a variable for current x-speed of the ball
    v_x_k = 0
    ## Initializes a variable for current y-speed of the ball
    v_y_k = 0
    ## Initializes a variable for the alpha filter of ball position
    alpha = 0.85
    ## Initializes a variable for the beta filter of ball position
    beta = 0.005
    ## Initializes a variable for the predicted x-velocity of the ball
    v_x_k1 = 0
    ## Initializes a variable for the predicted y-velocity of the ball
    v_y_k1 = 0

    while True:
        #print('Task Panel Running...')
        ## Defines the current run time of the task
        current_time = time.ticks_us()
        if time.ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                #print('S0')
                print('Checking panel calibration...')
                if file_name in os.listdir():
                    state = S4_WRITE_COEFFICIENTS
                    
                else:
                    state = S2_CALIBRATE

            elif state == S1_UPDATE_PLATFORM:
                ## Defines a tuple containing the return values from the panel scan method
                (x, y, contact) = PANEL.scan() 
                if contact: 
                    ## Calculates the change in run time between each iteration of task panel
                    delta_t = time.ticks_diff(current_time, prev_time) / 1_000_000
                    x_k1 = x_k + (alpha * (x - x_k)) + (delta_t * v_x_k)
                    v_x_k1 = v_x_k + ((beta / delta_t) * (x - x_k))
                    
                    x_k = x_k1
                    v_x_k = v_x_k1
                    
                    y_k1 = y_k + (alpha * (y - y_k)) + (delta_t * v_y_k)
                    v_y_k1 = v_y_k + ((beta / delta_t) * (y - y_k))
                    
                    y_k = y_k1
                    v_y_k = v_y_k1

                elif not contact:
                    x_k1 = 0
                    y_k1 = 0
                    v_x_k1 = 0
                    v_y_k1 = 0

                # Writes pertinent ball velocity and position data to the shares variables
                ballPos.write((x_k1, y_k1))
                ballVel.write((v_x_k1, v_y_k1))
                #print(ballPos.read())
                #print(ballVel.read())
                #print(f'{x}     {v_x}       {y}     {v_y}')

            elif state == S2_CALIBRATE:
                #print('S2')
                print('No panel calibration file available. \n     Entering calibration setup...')
                print('Follow the instructions to touch the panel at appropreiate locations:')
                pyb.delay(3_000)
                ## Initializes a NumPy array to contain values of the touch panel calibration matrix
                calMatrix = np.ones((5,3))
                ## Initializes a NumPy array to contain raw ADC values of the touch panel calibration matrix
                ADCMatrix = np.ones((5,2))
                ## Initializes a counting variable to monitor the number of touches of the touch panel
                touches = 0
                ## Initializes a boolean check flag to increment successive user touches of the panel
                check = False

                while(touches < 5):
                    ## Defines a tuple of the ADC position of the ball in x-y and the z-contact pressure of the ball
                    (ADC_x, ADC_y, contact) = PANEL.scan()
                    if contact and check:
                        ADCMatrix[touches,:] = [ADC_x, ADC_y]
                        check = False 
                        touches += 1
                    elif not contact and not check:
                        check = True
                        pyb.delay(200)
                        print('Touch the panel at position ({}, {}) mm'.format(calibrationPoints[touches, 0], calibrationPoints[touches, 1]))

                # Transfers the ADC matrix to my calibration matrix
                calMatrix[:,0:2] = ADCMatrix
                ## Defines a transposed matrix of the calibration matrix
                transpose_calMatrix = calMatrix.transpose()
                ## Defines the touch panel calibration matrix using the matrix algebra we derived in class
                calibrationMatrix = np.dot(np.linalg.inv(np.dot(transpose_calMatrix,calMatrix) ), np.dot(transpose_calMatrix, calibrationPoints))
                print(calibrationMatrix)
                ## Assigns the calibration constant in for the x-x term
                K_xx = calibrationMatrix[0][0]
                ## Assigns the calibration constant in for the y-x term
                K_yx = calibrationMatrix[0][1]
                ## Assigns the calibration constant in for the x-y termv
                K_xy = calibrationMatrix[1][0]
                ## Assigns the calibration constant in for the y-y term
                K_yy = calibrationMatrix[1][1]
                ## Assigns the calibration constant for the x-direction offset
                x_0 =  calibrationMatrix[2][0]
                ## Assigns the calibration constant for the y-direction offset
                y_0 =  calibrationMatrix[2][1]
                ## Assigns calibration constants to a tuple
                cals = (K_xx, K_yx, K_xy, K_yy, x_0, y_0)
                print(f'Panel Calibration Coefficients: {cals}')
                # Sets the calibration constants for the panel
                PANEL.set_calibration(cals)
                state = S3_SAVE_CALIBRATION
                '''
                ADC_x, ADC_y, contact = PANEL.scan()
                cal_pos = CAL_POS_1
                print('Touch at 0, 0')
                if cal_pos == CAL_POS_1:
                    if contact:
                        ADC_x1, ADC_y1, contact1 = ADC_x, ADC_y, contact
                        if not contact1:
                            print('Touch at 50, -80')
                            cal_pos = CAL_POS_2

                elif cal_pos == CAL_POS_2:
                    if contact:
                        ADC_x2, ADC_y2, contact2 = ADC_x, ADC_y, contact
                        if not contact2:
                            print('Touch at 50, 80')
                            cal_pos = CAL_POS_3

                elif cal_pos == CAL_POS_3:
                    if contact:
                        ADC_x3, ADC_y3, contact3 = ADC_x, ADC_y, contact
                        if not contact3:
                            print('Touch at -50, 80')
                            cal_pos = CAL_POS_3
                
                elif cal_pos == CAL_POS_4:
                    if contact:
                        ADC_x4, ADC_y4, contact4 = ADC_x, ADC_y, contact
                        if not contact4:
                            print('Touch at -50, -80')
                            cal_pos = CAL_POS_4

                elif cal_pos == CAL_POS_5:
                    if contact:
                        ADC_x5, ADC_y5, contact5 = ADC_x, ADC_y, contact
                        if not contact5:
                            print('Assembling calibration coefficients')
                            state = S3_SAVE_CALIBRATION
                '''

            elif state == S3_SAVE_CALIBRATION:
                #print('S3')
                '''calMatrix = np.array([ [ADC_x1, ADC_y1, 1], [ADC_x2, ADC_y2, 1], [ADC_x3, ADC_y3, 1], [ADC_x4, ADC_y4, 1], [ADC_x5, ADC_y5, 1] ])
                transpose_calMatrix = calMatrix.T
                calibrationMatrix = np.dot(np.linalg.inv(np.dot(transpose_calMatrix, calMatrix) ), np.dot(transpose_calMatrix, calibrationPoints))
                print(calibrationMatrix)
                K_xx = calibrationMatrix[0][0]
                K_yx = calibrationMatrix[0][1]
                K_xy = calibrationMatrix[1][0]
                K_yy = calibrationMatrix[1][1]
                x_0 =  calibrationMatrix[2][0]
                y_0 =  calibrationMatrix[2][1]
                cals = (K_xx, K_yx, K_xy, K_yy, x_0, y_0)
                #gc.collect()
                print(cals)
                PANEL.set_calibration(cals)
                print('HERE')
                time.sleep_us(200_000)
                print('HERE')
                state = S3_SAVE_CALIBRATION
                print('HERE')'''
                ## Initializes a en empty string to build text file data
                cal_string = ''
                for element in cals:
                    cal_string += str(element)
                    cal_string += ','
                cal_string = cal_string[:-1]
                with open(file_name, 'w') as file:
                    file.write(cal_string)
                print(f'Panel calibration data written to file {file_name}')
                state = S1_UPDATE_PLATFORM

            elif state == S4_WRITE_COEFFICIENTS:
                #print('S4')
                print('Panel calibration data exists! \n     Extracting calibration data...')
                #pyb.delay(3_000)
                with open(file_name, 'r') as file:
                    ## Assigns a string for reading for the calibration file
                    cal_data_string = file.readline()
                
                ## Assigns a data string to split the read values from the calibration file
                cal_data = cal_data_string.split(',')
                #print(cal_data)
                ## Defines a list for allocating calibration coefficients read from coefficients file
                cal_coeffs = 6*[0]
                #print(cal_coeffs)
                for i in range(0, len(cal_data)):
                    #print(cal_data[i])
                    cal_coeffs[i] = float(cal_data[i])
                print('Panel calibration coefficients extracted!')
                PANEL.set_calibration(cal_coeffs)
                state = S1_UPDATE_PLATFORM

            else:
                pass

            next_time = time.ticks_add(next_time, period)
            prev_time = current_time

            yield state

        else:
            yield None
