'''!@file       innerLoop_0xFF.py
    @brief      A driver for defining the inner closed loop object of the ball balancing control system.
    @details    This file consitutes a class that defines a closed loop motor calculation.
                This object accepts positional, velocity, and initial values to determine
                closed-loop motor control in the form of an adjusted duty cycle.
    @author     Jarod Lyles
    @date       March 17th, 2022
'''
class InnerLoop:
    '''!@brief   An inner loop class for the calculation of adjusted duty cycle..
        @details Creates an inner loop object for the balance ball project accepting
                 reference angle, euler angles, and gyrscope velocities.
                 
    '''
    def __init__(self):
        '''!@brief      Constructs a inner loop object.
            @details    This method initializes variables of an object, namely the adjusted duty cycle value and respective gain values.             
        '''
        ## Initializes an attribute for the inner loop proportional control gain.
        self.K_p = 0
        ## Initializes an attribute for the inner loop derivative control gain.
        self.K_d = 0
        ## Initializes an attribute for the adjust duty cycle calculation.
        self.L = 0

    def update(self, eulerAngle, gyrVel, setPoint):
        '''!@brief      Performs update of the inner closed loop system.
            @details    This method performs update of the inner closed loop system, accepting
                        positional, velocity, and set point data to do calculation of adjusted duty cycle.             
        '''
        ## Assigns an attribute for the positional theta values
        self.theta = eulerAngle
        ## Assigns an attribute for the angular velocity values
        self.theta_dot = gyrVel
        ## Assigns an attribute for the positional theta set point value
        self.theta_ref = setPoint
        ## Assigns an attribute for the initial angular velocity values
        self.theta_dot_ref = 0
        
        # Calculates the closed-loop adjusted duty cycle
        self.L = (self.K_p * (self.theta_ref - self.theta)) + (self.K_d * (self.theta_dot_ref - self.theta_dot)) # - (self.K_i * (self.) 
        
        # Saturation limits
        if self.L > 45:
            self.L = 45
        elif self.L < -45:
            self.L = -45
        else:
            self.L = self.L

        #print(f'theta {self.theta_ref}    duty {self.L}')
        return self.L

    def set_gain(self, K_p, K_d, K_i, comp):
        '''!@brief      Sets the inner closed-loop gains of the system.
            @details    This method assigns new values to the proportional, derivative, and integral
                        gain control constants of the system.             
        '''
        ## Assigns an attribute for the proportional gain
        self.K_p = K_p
        ## Assigns an attribute for the derivative gain
        self.K_d = K_d
        ## Assigns an attribute for the integral gain
        self.K_i = K_i
        ## Assigns an attribute for duty compensation
        self.comp = comp
    
    def get_gain(self):
        '''!@brief      Returns the value of the inner closed-loop gain variables.
            @details    This method returns values of the proportional, derivative, and integral
                        gain control constants.             
        '''
        return (self.K_p, self.K_d, self.K_i)

