'''!@file       main_0xFF.py
    @brief      A testing program for balancing the inverted pendulum platform.
    @details    The main file executes the necessary tasks to carry out the program
                assigns necessary shared variables amongst source code files.
    @author     Jarod Lyles
    @date       March 15, 2022
'''

import shares_0xFF, pyb, motor_0xFF
import task_user_0xFF, task_motor_0xFF, task_IMU_0xFF, task_panel_0xFF

## Assigns a shared boolean flag to manage positional data collection
rFlag = shares_0xFF.Share(False)
## Assigns a shared boolean flag to manage velocity data return
vFlag = shares_0xFF.Share(False)
## Assigns a shared boolean flag to manage position data return
lFlag = shares_0xFF.Share(False)
## Assigns a shared boolean flag to manage derivative control gain for inner loop
diFlag = shares_0xFF.Share(False)

## Assigns a shared boolean flag to manage derivative control gain for outer loop
doFlag = shares_0xFF.Share(False)

## Assigns a shared boolean flag to manage proportional control gain for inner loop
piFlag = shares_0xFF.Share(False)
## Assigns a shared boolean flag to manage proportional control gain for outer loop
poFlag = shares_0xFF.Share(False)

## Assigns a shared variable for motor 1 duty cycle
duty1 = shares_0xFF.Share(0)
## Assigns a shared variable for motor 2 duty cycle
duty2 = shares_0xFF.Share(0)

## Assigns a shared tuple for Euler angles
eulerAngles = shares_0xFF.Share((0, 0, 0))
## Assigns a shared tuple for angular velocities
gyrVels = shares_0xFF.Share((0, 0, 0))

## Assigns a shared tuple for calibration status
calStat = shares_0xFF.Share((0, 0, 0, 0))
## Assigns a shared variable for ball position
ballPos = shares_0xFF.Share((0, 0))
## Assigns a shared variable for ball velocity
ballVel = shares_0xFF.Share((0, 0))
## Assigns a shared variable for proportional gain for outer loop
K_p_outer = shares_0xFF.Share(0.2)
## Assigns a shared variable for derivative gain for outer loop
K_d_outer = shares_0xFF.Share(0.0)

## Assigns a shared variable for proportional gain for outer loop
K_p_inner = shares_0xFF.Share(5)
## Assigns a shared variable for derivative gain for inner loop
K_d_inner = shares_0xFF.Share(0.25)


## Initializes pin B4 on the Nucleo for interface with the first motor
pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
## Initializes pin B5 on the Nucleo for interface with the first motor
pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
## Initializes pin B0 on the Nucleo for interface with the second motor
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
## Initializes pin B6 on the Nucleo for interface with the second motor
pinB1 = pyb.Pin(pyb.Pin.cpu.B1)

## Initializes Motor 1 object using the motor class object
motor_1 = motor_0xFF.Motor(20_000, 1, pinB4, 2, pinB5)
## Initializes Motor 2 object using the motor class object
motor_2 = motor_0xFF.Motor(20_000, 3, pinB0, 4, pinB1)

## Defines the period of operation of the task functions in microseconds
period = 10_000

if __name__ == '__main__':
    
    ## Initializes the user task
    task1 = task_user_0xFF.taskUser('taskUser', period, eulerAngles, gyrVels, diFlag, doFlag, piFlag, poFlag, lFlag, vFlag, rFlag, K_p_outer, K_p_inner, K_d_outer, K_d_inner, ballPos, ballVel)
    ## Initializes the touch panel task
    task2 = task_panel_0xFF.taskPanel('taskPanel', period, ballPos, ballVel)
    ## Initializes the IMU task
    task3 = task_IMU_0xFF.taskIMU('taskIMU', period, eulerAngles, gyrVels, calStat)
    ## Initializes the motor 1 / controller 1 task
    task4 = task_motor_0xFF.taskMotor('taskMotor1', period, motor_1, duty1, K_p_outer, K_p_inner, K_d_outer, K_d_inner, eulerAngles, gyrVels, 1, ballPos, ballVel)
    ## Initializes the motor 2 / controller 2 task
    task5 = task_motor_0xFF.taskMotor('taskMotor2', period, motor_2, duty2, K_p_outer, K_p_inner, K_d_outer, K_d_inner, eulerAngles, gyrVels, 2, ballPos, ballVel)
    ## Defines the task list to be run
    taskList = [task1, task2, task3, task4, task5]
    
    while True:
        try:
            for task in taskList:
                next(task)
        
        except KeyboardInterrupt:
            break
        
        
print('PROGRAM TERMINATING')