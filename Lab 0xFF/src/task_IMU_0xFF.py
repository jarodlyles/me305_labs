'''!@file       task_IMU_0xFF.py
    @brief      A module task for operating the BNO055 driver.
    @details    This file contains a task for properly managing the BNO055 driver.
                The task checks the status of the calibration, calibrates if needed, and 
                returns Euler angles and angular velocities. Alternatively, if calibration
                is not needed, calibration coefficients are pulled from an existing calibration
                constants file.
    @author     Jarod Lyles
    @date       March 5, 2022
'''

import pyb, time, BNO055_0xFF
import micropython, os, gc

## Defines a constant for State 0 of IMU task
S0_INIT = micropython.const(0)
## Defines a constant for State 1 of IMU task
S1_UPDATE_ANGLES_VELS = micropython.const(1)
## Defines a constant for State 2 of IMU task
S2_CALIBRATE = micropython.const(2)
## Defines a constant for State 3 of IMU task
S3_SAVE_CALIBRATION = micropython.const(3)
## Defines a constant for State 4 of IMU task
S4_WRITE_COEFFICIENTS = micropython.const(4)

def taskIMU(taskName, period, eulerAngles, gyrVels, calStat):
    '''!@brief           The module tasks the BNO055 according to state conditions.
        @details         This task accepts shared variables positional, velocity, and 
                         calibration details to operate the IMU and ultimately return
                         shared Euler angles and gyroscope velocities.
                         appropreiate state transitions and return values to
                         to the ueser interface.
        @param taskName  Assigns a name to the taskUser task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param eulerAngles  Initializes a passed shared variable for Euler angles.
        @param gyrVels   Initializes a passed shared variable for gyroscope velocities.
        @param calStat   Initializes a passed shared calibration status to check if the IMU
                         is calibrated.
                         
    '''
    ## Initializes a BNO055 IMU object
    IMU = BNO055_0xFF.BNO055()
    ## Initializes the start time of the task
    start_time = time.ticks_us()
    ## Initializes the next time of the task
    next_time = time.ticks_add(start_time, period)
    #print(IMU.get_euler())
    #print(IMU.get_omega())
    #print(IMU.get_cal_status())

    ## Defines the file name for reading/writing calibration coefficients
    file_name = "IMU_cal_coeffs.txt"
    ## Defines a variable for the state of the system
    state = S0_INIT

    while True:
        #print('Task IMU Running...')
        ## Defines the current run time of the task
        current_time = time.ticks_us()
        if time.ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                print('Checking calibration...')
                if file_name in os.listdir():
                    state = S4_WRITE_COEFFICIENTS
                    
                else:
                    IMU.change_mode(12)
                    print('No IMU calibration file available. \n     Entering calibration setup...')
                    print('\nManipulate the platform until all IMU units measure 3\n')
                    print('IMU Calibration will begin in 3 seconds\n')
                    pyb.delay(3000)
                    state = S2_CALIBRATE
                

            elif state == S1_UPDATE_ANGLES_VELS:
                eulerAngles.write(IMU.get_euler())
                gyrVels.write(IMU.get_omega())
                '''
                print(euler_angles)
                print(gyr_vels)
                print("Run!")
                break
                '''

            elif state == S2_CALIBRATE:
                calStat.write(IMU.get_cal_status())
                ## Reads and assigns the according values of the calibration status
                mag, acc, gyr, sys = calStat.read()
                '''
                if mag == 3:
                    print('Magnetometer calibrated!')
                if acc == 3:
                    print('Accelerometer calibrated!')
                if gyr == 3:
                    print('Gyroscope calibrated!')
                if sys == 3:
                    print('System calibrated!')
                '''
                print(f'Magnetometer: {mag} | Accelerometer: {acc} | Gyroscope: {gyr} | System: {sys}')
                
                #print(f'Magnetometer:    {mag}', end = "")
                '''print(f'Accelerometer:   {acc}')
                print(f'Gyroscope:       {gyr}')
                print(f'System:          {sys}')
                '''
                if (mag, acc, gyr, sys) == (3, 3, 3, 3):
                    print('\nIMU Calibration done!')
                    state = S3_SAVE_CALIBRATION

            elif state == S3_SAVE_CALIBRATION:
                ## Assigns a bytearray for allocating calibration data
                buff = bytearray(22*[0])
                gc.collect()
                ## Calls the IMU object to get calibration coefficients
                cal_data = IMU.get_cal_coeff(buff)
                #print(cal_data)
                ## Defines a calibration coefficient list for writing to a text file
                cal_string = ''
                for byte in cal_data:
                    cal_string += hex(byte)
                    cal_string += ','
                cal_string = cal_string[:-1]
                #print("append")
                with open(file_name, 'w') as file:
                    file.write(cal_string)
                #print("written")
                print(f'IMU calibration data written to file {file_name}')
                state = S1_UPDATE_ANGLES_VELS


            elif state == S4_WRITE_COEFFICIENTS:
                print('IMU calibration data exists! \n     Extracting calibration data...')
                #pyb.delay(3_000)
                with open(file_name, 'r') as file:
                    ## Assigns a string for reading for the calibration file
                    cal_data_string = file.readline()
                
                ## Assigns a data string to split the read values from the calibration file
                cal_data = cal_data_string.split(',')
                ## Defines a byteaarray for allocating calibration coefficients read from coefficients file
                cal_coeffs = bytearray(22*[0])
                #print(cal_coeffs)
                for i in range(0, len(cal_data)):
                    cal_coeffs[i] = int(cal_data[i])
                
                #print(cal_coeffs)
                
                IMU.change_mode(0)
                IMU.set_cal_coeff(cal_coeffs)
                IMU.change_mode(12)
                #print('S4 -> S1')
                print('IMU calibration coefficients extracted!')
                state = S1_UPDATE_ANGLES_VELS
            
            else:
                pass

            next_time = time.ticks_add(next_time, period)

            yield state

        else:
            yield None
    