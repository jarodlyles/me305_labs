'''!@file       task_motor_0xFF.py
    @brief      A module for tasking the motor to enable the driver DRV8847.
    @details    This module initializes motor objects and tasks the motor to the appropreiate
                frequency of operation.          
    @author     Jarod Lyles
    @date       February 2, 2022
'''

import time
import innerLoop_0xFF, outerLoop_0xFF

## Instantiates an object for the inner loop controller
innerLoopController = innerLoop_0xFF.InnerLoop()
## Instantiates an object for the outer loop controller
outerLoopController = outerLoop_0xFF.OuterLoop()

def taskMotor(taskName, period, motor, duty, K_p_outer, K_p_inner, K_d_outer, K_d_inner, eulerAngles, gyrVels, motorNumber, ballPos, ballVel):
    '''!@brief           Manages the initialization and tasking of the motor drivers.
        @details         This task, taskMotor(), accepts shared variables
                         for boolean flags and duty cycle details to perform 
                         PWM drive on the motors.
        @param taskName  Assigns a name to the instance of the taskMotor task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param K_p_outer Passed variable for outer loop proportional gain value.
        @param K_p_inner Passed variable for inner loop proportional gain value.
        @param K_d_outer Passed variable for outer loop derivative gain value.
        @param K_d_inner Passed variable for inner loop derivative gain value.
        @param duty      Assigns a value for a shared object containing the requested
                         PWM signal.
        @param motor     Passes a motor object to assigning duty cycle.
        @param duty       Initializes a passed duty shared variable to write to.
        @param eulerAngles Initializes a passed shared dataset for Euler angles.
        @param gyrVels     Initializes a passed shared dataset for angular velocities.
        @param setPoint    Initializes a passed shared variable for positional setpoint.
        @param motorNumber Defines the number of the motor in the system.
        @param ballPos     Passes a shared value for the current position of the ball.
        @param ballVel     Passes a shared value for the current position of the ball.
    '''

    ## Initializes start time for the task encoder function
    start_time = time.ticks_us()
    ## Initializes first iteration time interval of update for the task encoder
    next_time = time.ticks_add(start_time, period)

    while True:
        #print('Task Motor Running...')
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()

        if time.ticks_diff(current_time, next_time) >= 0:
            if motorNumber == 1:
                outerLoopController.set_gain(K_p_outer.read(), K_d_outer.read(), 0, 0.1)
                innerLoopController.set_gain(K_p_inner.read(), K_d_inner.read(), 0, 0)

            if motorNumber == 2:
                outerLoopController.set_gain(-1 * K_p_outer.read(), -1 * K_d_outer.read(), 0, 0.1)
                innerLoopController.set_gain(1 * K_p_inner.read(), 1 * K_d_inner.read(), 0, 3)

            ## Reads the shared position to a local number
            x = ballPos.read()[motorNumber-1]
            ## Reads the shared velocity to a local number
            x_dot = ballVel.read()[motorNumber-1]
            
            ## Reads the platform angle to a local variable
            theta = eulerAngles.read()[motorNumber]
            ## Reads the platform angular velocity to a local variable
            theta_dot = gyrVels.read()[motorNumber]
            '''
            if motorNumber == 1:
                print(f'X: {x} | X_d: {x_dot} | Theta: {theta} | Theta_d: {theta_dot}')
            '''
            ## Assigns a local variable for the culated reference agnle for the platform
            theta_ref = outerLoopController.update(x, x_dot)
            
            ## Assigns a local variable for the calculated duty cycle
            L = innerLoopController.update(theta, theta_dot, theta_ref)
            #closedLoopController.set_gain(K_p.read(), K_d.read(), K_i.read())
            duty.write(L)
            #print(L)
            #print(f'Duty {motorNumber}: {duty.read()}')
            #print(f'Motor {motorNumber} Setpoint : {outerLoopController.theta}')
            
            motor.set_duty(duty.read())
            #print(duty.read())
            # Provides update time for the loop
            next_time = time.ticks_add(next_time, period)

        else:
            yield None
        
        
