'''!@file       task_user_0xFF.py
    @brief      A module task for managing state transitions and user interface.
    @details    This file contains a user interface in the form of a task to interpret user 
                keyboard inputs and process conditionals to perform the actions 
                of the state transitions.
    @author     Jarod Lyles
    @date       February 17, 2022
'''


import pyb, time, array, micropython
from shares_0xFF import Share
import gc

## Defines constant for State 0
S0_INIT = micropython.const(0)
## Defines constant for State 1
S1_WAIT_FOR_CMD = micropython.const(1)
## Defines constant for State 2
S2_ACCEPT_DATA_INPUT = micropython.const(2)
## Defines constant for State 3
S3_PRINT_POSITION = micropython.const(3)
## Defines constant for State 4
S4_GET_VELOCITY = micropython.const(4)
## Defines constant for State 5
S5_INNER_OUTER = micropython.const(5)
## Defines constant for State 6
S6_INNER_OUTER = micropython.const(6)
## Defines constant for State 7
S7_RECORD_DATA = micropython.const(7)

## Initializes array for storing collected time data
timeArray = array.array('f', 1000 * [0])
gc.collect()
    
## Initializes array for storing collected position data
posArray = array.array('f', timeArray)

## Initializes a serial port reader for user input
serport = pyb.USB_VCP()
        
def taskUser(taskName, period, eulerAngles, gyrVels, diFlag, doFlag, piFlag, poFlag, lFlag, vFlag, rFlag, K_p_outer, K_p_inner, K_d_outer, K_d_inner, ballPos, ballVel):

    '''!@brief           Manages the user interfacing, state transitions, and
                         returning data to the user.
        @details         This task, taskUser(), accepts shared variables
                         for boolean flags and positional details to perform 
                         appropreiate state transitions and return values to
                         to the user interface.
        @param taskName  Assigns a name to the taskUser task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param position  Initializes a passed shared integer variable upon which encoder
                         position data can be assigned.
        @param eulerAngles  Initializes a passed shared variable for Euler angles.
        @param gyrVels   Initializes a passed shared variable for gyroscope velocities.
        @param K_p       Initializes a passed shared variable for proportional gain.
        @param K_d       Initializes a passed shared variable for derivative gain.
        @param XsetPoint Initializes a passed shared variable for x-theta set point.
        @param YsetPoint Initializes a passed shared variable for y-theta set point.
        @param pFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for proportional gain.
        @param dFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for derivative gain.
        @param lFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for Euler angles.
        @param vFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for angular velocities.
        @param xFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for x-theta set point.
        @param yFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for y-theta set point.
                         
    '''
    
    intro()
    
    ## Initializes an active state variable for transition management
    state = S0_INIT
    ## Initializes start time for the task encoder function
    start_time = time.ticks_us()
    ## Initializes time interval of update for the task encoder
    next_time = time.ticks_add(start_time, period)
    
    while True:
        #print('Task User Running...')
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()
        if time.ticks_diff(current_time,next_time) >= 0:
            next_time = time.ticks_add(next_time, period)
            
            if state == S0_INIT:
                state = S1_WAIT_FOR_CMD
                    
            elif state == S1_WAIT_FOR_CMD:
                if serport.any():
                    
                    ## Assigns a character variable for the read out from the serial coms
                    charIn = serport.read(1).decode()
                    
                    # The following if/else tree performs the conditional logic
                    # on the user input to determine state transtion from State 1 
                    # and properly flag for use in the task encoder.
                        
                    if charIn in {'l', 'L'}:
                        state = S3_PRINT_POSITION
                        lFlag.write(True)
                        print('Input L – Returning Platform Position and Ball Position')
                        print('_____________________________________________________________')

                    elif charIn in {'v', 'V'}:
                        state = S4_GET_VELOCITY
                        print('Input V - Get Gryroscope Velocity and Ball Velocity')
                        print('_____________________________________________________________')
                        vFlag.write(True)

                    elif charIn in {'p', 'P'}:
                        print('Enter [I] for the inner loop or [O] for the outer loop')
                        print('_____________________________________________________________')
                        state = S5_INNER_OUTER
                    
                    elif charIn in {'d', 'D'}:
                        print('Enter [I] for the inner loop or [O] for the outer loop')
                        print('_____________________________________________________________')
                        state = S6_INNER_OUTER
                    
                    elif charIn in {'h', 'H'}:
                        intro()
                        state = S1_WAIT_FOR_CMD

                    elif charIn in {'r', 'R'}:
                        print('Recording Ball Position Data')
                        print('_____________________________________________________________')
                        ## Defines the start of data collection
                        collection_start = time.ticks_ms()
                        ## Defines the beginning index of data collection
                        index = 0
                        
                        print('            Begin Data Collection             ')
                        print('______________________________________________')
                        print('  Time [s], X-Position [mm], Y-Position [mm]  ')
                        rFlag.write(True)
                        state = S7_RECORD_DATA

                    else:
                        print(f'You entered {charIn}. This is not a valid command.')
            
            elif state == S5_INNER_OUTER:
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'i', 'I'}:
                        state = S2_ACCEPT_DATA_INPUT
                        print('Input K - Enter Proportional Gain (K_pi) for Inner-Loop Control')

                        print('_____________________________________________________________')
                        piFlag.write(True)
                        buff = ''
                    elif charIn in {'o', 'O'}:
                        state = S2_ACCEPT_DATA_INPUT
                        print('Input K - Enter Proportional Gain (K_po) for Outer-Loop Control')
                        print('_____________________________________________________________')
                        poFlag.write(True)
                        buff = ''

            elif state == S6_INNER_OUTER:
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'i', 'I'}:
                        state = S2_ACCEPT_DATA_INPUT
                        print('Input D - Enter Proportional Gain (K_di) for Inner-Loop Control')
                        print('_____________________________________________________________')
                        diFlag.write(True)
                        buff = ''
                    elif charIn in {'o', 'O'}:
                        state = S2_ACCEPT_DATA_INPUT
                        print('Input D - Enter Proportional Gain (K_do) for Outer-Loop Control')
                        print('_____________________________________________________________')
                        doFlag.write(True)
                        buff = ''

            # The following if/else tree determines appropreiate actions per 
            # state assignment
            elif state == S2_ACCEPT_DATA_INPUT:
                if serport.any():
                    charIn = serport.read(1).decode()

                    if charIn.isdigit():
                        buff += charIn
                        print(charIn, end='' )
                        
                    elif charIn in {'-'}: 
                        if len(buff) == 0:
                            buff += charIn
                            print(charIn, end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}: 
                        if len(buff) > 0:
                            buff = buff[:-1]
                            print('')
                            print(buff)
                        elif len(buff) == 0:
                            pass
                            
                    elif charIn in {'.'}:
                        buff += charIn
                        print(charIn, end='')
                        
                    elif charIn in {'\n','\r'}:
                        print('\n')
                        if len(buff) == 0:
                            print('Invalid input. Please retry.')
                            state = S1_WAIT_FOR_CMD
                        
                        if poFlag.read():
                            print('_____________________________________________________________')
                            K_p_outer.write(float(buff))
                            print(f'Outer-loop proportional control gain set to {K_p_outer.read()}')
                            print('_____________________________________________________________')
                            poFlag.write(False)
                            state = S1_WAIT_FOR_CMD
                        
                        if piFlag.read():
                            print('_____________________________________________________________')
                            K_p_inner.write(float(buff))
                            print(f'Inner-loop proportional control gain set to {K_p_inner.read()}')
                            print('_____________________________________________________________')
                            piFlag.write(False)
                            state = S1_WAIT_FOR_CMD

                        if doFlag.read():
                            print('_____________________________________________________________')
                            K_d_outer.write(float(buff))
                            print(f'Outer-loop derivative control gain set to {K_d_outer.read()}')
                            print('_____________________________________________________________')
                            doFlag.write(False)
                            state = S1_WAIT_FOR_CMD
                        
                        if diFlag.read():
                            print('_____________________________________________________________')
                            K_d_inner.write(float(buff))
                            print(f'Inner-loop derivative control gain set to {K_d_inner.read()}')
                            diFlag.write(False)
                            state = S1_WAIT_FOR_CMD

                
            elif state == S3_PRINT_POSITION:
                if lFlag.read():
                    (head, pitch, roll) = eulerAngles.read()
                    (ball_x, ball_y) = ballPos.read()
                    print('Platform Angular Positions')
                    print(f'Head:  {head}°')
                    print(f'Pitch: {pitch}°')
                    print(f'Roll:  {roll}°')
                    print('_____________________________________________________________')
                    print('Ball X-Y Position')
                    print(f'X-Position: {ball_x}mm')
                    print(f'Y-Position: {ball_y}mm')
                    print('_____________________________________________________________')
                    lFlag.write(False)
                    state = S1_WAIT_FOR_CMD
            
            elif state == S4_GET_VELOCITY:
                if vFlag.read():
                    (x_vel, y_vel, z_vel) = gyrVels.read()
                    (ball_vel_x, ball_vel_y) = ballVel.read()
                    print('Platform Angular Velocities')
                    print(f'About X-axis: {x_vel}°/s')
                    print(f'About Y-axis: {y_vel}°/s')
                    print(f'About Z-axis: {z_vel}°/s')
                    print('_____________________________________________________________')
                    print('Ball Linear Velocities')
                    print(f'X-Component: {ball_vel_x}mm/s')
                    print(f'Y-Component: {ball_vel_y}mm/s')
                    print('_____________________________________________________________')
                    vFlag.write(False)
                    state = S1_WAIT_FOR_CMD

            elif state == S7_RECORD_DATA:
                if rFlag.read():
                    # Reads the serial port for potential 'S' input
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn in {'s', 'S'}:
                            rFlag.write(False)
                            # For loop to print out time/position data if 'S' is entered
                            for (t, p, v) in zip(timeArray[:index], posArray[:index]):
                                print(f'{(t - collection_start)/1000:.2f},  {p},    {v}')
                                #print(f'{(t - collection_start)/1000:.2f}, {p}')

                            print('______________________________________________')
                            print('             End Data Collection              ')
                            state = S1_WAIT_FOR_CMD
                        
                    # Conditional for 1000 data entries (10 seconds given 1000us period)
                    elif index >= 1000:
                        # For loop to print out time/position data after 1000 entries (30 seconds)
                        rFlag.write(False)
                        for (t, p) in zip(timeArray[:index], posArray[:index]):
                            print(f'{(t - collection_start)/1000:.2f},  {p},    {v}')
                            #print(f'{(t - collection_start)/1000:.2f}, {p}')
                            
                        print('______________________________________________')
                        print('             End Data Collection              ')
                        state = S1_WAIT_FOR_CMD
                    
                    # Conditional to continue adding to the time/position array otherwise
                    else:
                        timeArray[index] = time.ticks_us()
                        posArray[index] = ballPos.read()
                        index += 1
                    
            else:
                raise ValueError(f'Invalid state value in {taskName}: State {state} does not exist.')
                
            yield state
            
        else:
            yield None
    

# Function for printing the welcome / command window
def intro():
    print(' _____________________________________________________ ')
    print('|                                                     |')
    print('|               BALL BALANCING INTERFACE              |')
    print('|_____________________________________________________|')
    print('| Available user commands:                            |')
    print('|  [L] or [l]   |   Print Platform & Ball position    |')
    print('|  [V] or [v]   |   Print Platform & Ball velocity    |')
    print('|  [P] or [p]   |   Set the proportional control gains|')
    print('|  [D] or [d]   |   Set the derivative control gains  |')
    print('|  [R] or [r]   |   Record ball position data         |')
    print('|  [H] or [h]   |   Retrieves the help menu           |')
    print('|  [CTRL]+[C]   |   Interrupt program                 |')
    print('|_____________________________________________________|\n')
