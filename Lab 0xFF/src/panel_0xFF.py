'''!@file       panel_0xFF.py
    @brief      A driver for panel ADC read-out.
    @details    This class defines a panel object that can be used to read the position of the ball on the touch panel.
    @author     Jarod Lyles
    @date       February 17, 2022
'''

from pyb import Pin, ADC, delay
from micropython import const
from ulab import numpy as np
import time, os, gc


class TouchPanel():
    '''!@brief      A class to create touch panel objects.
        @details    Class can be used to apply read touch panel position of contacting object
    '''
    def __init__(self):
        '''!@brief      Initializes a touch panel object
            @details    This initialization function creates the necessary attributes to operate the touch panel.
        '''
        ## Initializes an attribute for pin A0
        self._A0 = const(Pin.cpu.A0)
        ## Initializes an attribute for pin A1
        self._A1 = const(Pin.cpu.A1)
        ## Initializes an attribute for pin A6
        self._A6 = const(Pin.cpu.A6)
        ## Initializes an attribute for pin A7
        self._A7 = const(Pin.cpu.A7)
        ## Initializes an attribute for pin output mode
        self._OUT = const(Pin.OUT_PP)
        ## Initializes an attribute for pin input mode
        self._IN = const(Pin.IN)
        ## Initializes a tuple for panel calibration constants
        (self.K_xx, self.K_xy, self.K_yx, self.K_yy, self.x_0, self.y_0) = (1, 0, 0, 1, 0, 0)

    def scan(self):
        '''!@brief      A touch panel class to 
            @details    Class can be used to apply read touch panel position of contacting object
            @return     The calibration corrected x- and y-component of ball position and a boolean for whether there is contact with the touch panel
        '''
        ## Initializes an ADC object for y-read using a pyb builtin library
        self._ADC_y = ADC(self._A1)
        ## Initializes an attribute for the y positive terminal of the ADC
        self._Y_p = Pin(self._A6, self._OUT)
        ## Initializes an attribute for the y negative terminal of the ADC    
        self._Y_m = Pin(self._A0, self._OUT)
        ## Initializes an attribute for the x positive terminal of the ADC
        self._X_p = Pin(self._A7, self._IN)
        self._Y_p.high()
        self._Y_m.low()
        ## Initializes an attribute for y-component ADC output
        self._Y_r = self._ADC_y.read()
    
        ## Initializes an ADC object for z-read using a pyb builtin library
        self._ADC_z = ADC(self._A7)
        ## Initializes an attribute for the x negative terminal of the ADC    
        self._X_m = Pin(self._A1, self._OUT)
        self._Y_m = Pin(self._A0, self._IN)
        self._Y_p.high()
        self._X_m.low()
        
        if self._ADC_z.read() > 100:
             ## Initializes an attribute for contact with the touch panel   
            self._contact = True
        else:
            self._contact = False
        
        ## Initializes an attribute for x-component ADC output
        self._ADC_x = ADC(self._A0)
        self._X_p = Pin(self._A7, self._OUT)
        self._Y_p = Pin(self._A6, self._IN)
        self._X_p.high()
        self._X_m.low()
        ## Initializes an attribute for x-component ADC output
        self._X_r = self._ADC_x.read()
        
        ## Performs calibration on the x-read from the ADC using calibration constants
        self._X = (self.K_xx * self._X_r) + (self.K_xy * self._Y_r) + self.x_0
        ## Performs calibration on the y-read from the ADC using calibration constants
        self._Y = (self.K_yy * self._Y_r) + (self.K_yx * self._X_r) + self.y_0

        return (self._X, self._Y, self._contact)

    def set_calibration(self, cal_coeff):
        '''!@brief      A method to set the calibration constants of the touch panel
            @details    This method takes in calirbation coefficients and sets appropreiate class attributes
        '''
        (self.K_xx, self.K_xy, self.K_yx, self.K_yy, self.x_0, self.y_0) = cal_coeff

'''
if __name__ == '__main__':
    print('run')
    ## Defines a constant for State 0 of IMU task
    S0_INIT = const(0)
    ## Defines a constant for State 1 of IMU task
    S1_UPDATE_PLATFORM = const(1)
    ## Defines a constant for State 2 of IMU task
    S2_CALIBRATE = const(2)
    ## Defines a constant for State 3 of IMU task
    S3_SAVE_CALIBRATION = const(3)
    ## Defines a constant for State 4 of IMU task
    S4_WRITE_COEFFICIENTS = const(4)

    calibrationPoints = np.array([[0, 0], [-80, 40], [80, 40], [80, -40], [-80, -40]])

    ## Initializes a BNO055 IMU object
    PANEL = TouchPanel()
    period = 10_000
    ## Initializes the start time of the task
    start_time = time.ticks_us()
    ## Initializes the next time of the task
    next_time = time.ticks_add(start_time, period)
    
    file_name = "Panel_cal_coeffs.txt"
    ## Defines a variable for the state of the system
    state = S0_INIT
    x_k = 0
    y_k = 0
    v_x_k = 0
    v_y_k = 0
    alpha = 0.85
    beta = 0.005
    v_x_k1 = 0
    v_y_k1 = 0
    print('run1')
    while True:
        ## Defines the current run time of the task
        current_time = time.ticks_us()
        if current_time >= next_time:
            
            if state == S0_INIT:
                print('S0')
                print('Checking calibration...')
                if file_name in os.listdir():
                    print('Calibration data exists! \n     Extracting calibration data...')
                    state = S4_WRITE_COEFFICIENTS
                    
                else:
                    print('No calibration file available. \n     Entering calibration setup...')
                    print('\nManipulate the platform until all IMU units measure 3\n')
                    print('Calibration will begin in 3 seconds\n')
                    #pyb.delay(3000)
                    state = S2_CALIBRATE
                

            elif state == S1_UPDATE_PLATFORM:
                (x, y, contact) = PANEL.scan() 
                if contact: 
                    delta_t = time.ticks_diff(current_time, prev_time) / 1_000_000
                    x_k1 = x_k + (alpha * (x - x_k)) + (delta_t * v_x_k)
                    v_x_k1 = v_x_k + ((beta / delta_t) * (x - x_k))
                    
                    x_k = x_k1
                    v_x_k = v_x_k1
                    
                    y_k1 = y_k + (alpha * (y - y_k)) + (delta_t * v_y_k)
                    v_y_k1 = v_y_k + ((beta / delta_t) * (y - y_k))
                    
                    y_k = y_k1
                    v_y_k = v_y_k1

                    

                elif not contact:
                    x = 0
                    y = 0
                    v_x = 0
                    v_y = 0

                #print(f'{x}     {v_x}       {y}     {v_y}')

            elif state == S2_CALIBRATE:
                print('S2')
                calMatrix = np.ones((5,3))
                ADCMatrix = np.ones((5,2))
                touches = 0
                check = False
                while(touches < 5):
                    ADC_x, ADC_y, contact = PANEL.scan()
                    if contact and check:
                        ADCMatrix[touches,:] = [ADC_x, ADC_y]
                        check = False 
                        touches += 1
                    elif not contact and not check:
                        check = True
                        time.sleep_us(200_000)
                        print(f'Touch the panel at position {calibrationPoints[touches]}')

                calMatrix[:,0:2] = ADCMatrix
                transpose_calMatrix = calMatrix.transpose()
                calibrationMatrix = np.dot(np.linalg.inv(np.dot(transpose_calMatrix, calMatrix) ), np.dot(transpose_calMatrix, calibrationPoints))
                print(calibrationMatrix)
                K_xx = calibrationMatrix[0][0]
                K_yx = calibrationMatrix[0][1]
                K_xy = calibrationMatrix[1][0]
                K_yy = calibrationMatrix[1][1]
                x_0 =  calibrationMatrix[2][0]
                y_0 =  calibrationMatrix[2][1]
                cals = (K_xx, K_yx, K_xy, K_yy, x_0, y_0)
                PANEL.set_calibration(cals)
                state = S3_SAVE_CALIBRATION

            elif state == S3_SAVE_CALIBRATION:
                print('S3')
                
                ## Assigns a bytearray for allocating calibration data
                string = ''
                with open(file_name, 'w') as file:
                    for coeff in cals:
                        string += str(coeff)
                        string += ','
                    string = string[:-1]
                    file.write(string)
                print(f'Calibration data written to file {file_name}')
                state = S1_UPDATE_PLATFORM

            elif state == S4_WRITE_COEFFICIENTS:
                print('S4')
                with open(file_name, 'r') as file:
                    ## Assigns a string for reading for the calibration file
                    cal_data_string = file.readline()
                
                ## Assigns a data string to split the read values from the calibration file
                cal_data = cal_data_string.split(',')
                print(cal_data)
                ## Defines a list for allocating calibration coefficients read from coefficients file
                cal_coeffs = 6*[0]
                #print(cal_coeffs)
                for i in range(0, len(cal_data)):
                    cal_coeffs[i] = float(cal_data[i])
                print('Calibration coefficients extracted!')
                PANEL.set_calibration(cal_coeffs)
                state = S1_UPDATE_PLATFORM

            else:
                pass

            next_time = time.ticks_add(start_time, period)
            prev_time = current_time

if __name__ == "__main__":
    panel = TouchPanel()
    start = time.ticks_us()
    panel.scan()
    end = time.ticks_us()
    print(panel.scan())
    print(f'{time.ticks_diff(end, start)} us scan time')
    touchCount = 0
    
    while True:
        P1Count, P2Count, P3Count, P4Count, P5Count, i = 0
        x, y, z = panel.scan()
        data_x, data_y = []

        if z and touchCount == 0:
            print("Touch the center")
            if z and P1Count < 10:
                data_x.append(x)
                data_y.append(y)
                i += 1
            touchCount += 1
'''