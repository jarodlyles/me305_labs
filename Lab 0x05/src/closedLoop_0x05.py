'''!@file       ClosedLoop_0x05.py
    @brief      A driver for defining a closed loop object
    @details    This file consitutes a class that defines a closed loop motor calculation.
                This object accepts positional, valocity, and initial values to determine
                closed-loop motor control.
    @author     Jarod Lyles
    @date       March 5th, 2022
'''
class ClosedLoop:
    '''!@brief      Defines a class for an closed loop object.
        @details    This class creates an object for closed loop control.        
    '''
    def init(self):
        '''!@brief      Constructs an closed loop object.
            @details    This method initializes variables of an object, namely the adjusted duty cycle value.             
        '''
        ## Initializes an attribute for the adjust duty cycle calculation.
        self.L = 0

    def update(self, eulerAngle, gyrVel, setPoint):
        '''!@brief      Performs update of the closed loop system.
            @details    This method performs update of the closed loop system, accepting
                        positional, velocity, and set point data to do calculation.             
        '''
        ## Assigns an attribute for the positional theta values
        self.theta = eulerAngle
        ## Assigns an attribute for the angular velocity values
        self.theta_dot = gyrVel
        ## Assigns an attribute for the positional theta set point value
        self.theta_ref = setPoint
        ## Assigns an attribute for the initial angular velocity values
        self.theta_dot_ref = 0
        
        # Calclates the closed-loop adjusted duty cycle
        self.L = (self.K_p * (self.theta_ref - self.theta)) + (self.K_d * (self.theta_dot_ref - self.theta_dot)) # - (self.K_i * (self.) 
        
        if self.L > 30:
            self.L = 30
        elif self.L < -30:
            self.L = -30
        
        #print(self.L)
        return self.L

    def set_gain(self, K_p, K_d, K_i):
        '''!@brief      Sets the closed-loop gains of the system.
            @details    This method assigns new values to the proportional, derivative, and integral
                        gain control constants of the system.             
        '''
        ## Assigns an attribute for the proportional gain
        self.K_p = K_p
        ## Assigns an attribute for the derivative gain
        self.K_d = K_d
        ## Assigns an attribute for the integral gain
        self.K_i = K_i
    
    def get_gain(self):
        '''!@brief      Returns the value of the closed loop gain variables.
            @details    This method returns values of the proportional, derivative, and integral
                        gain control constants.             
        '''
        return (self.K_p, self.K_d, self.K_i)

