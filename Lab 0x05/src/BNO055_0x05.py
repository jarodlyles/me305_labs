'''!@file       BNO055_0x05.py
    @brief      A driver for reading and writing BNO055 IMU data.
    @details    This file consitutes a driver to set up and retrieve data from 
                the BNO055 IMU. The methods provided below allow for the user to create
                an IMU object, change the operation mode of the device, retrieve the
                calibration status of the device, retrieve the calibration constants,
                write over the calibration constants, retrieve the Euler Angles, and
                retrieve the angular velocity of the device.
    @author     Jarod Lyles
    @date       March 5th, 2022
'''

from pyb import I2C, delay
import time, os, gc

class BNO055:
    '''!@brief An IMU driver class for the BNO055 from Bosch.
        @details Objects of this class can be used to configure the BNO055
                 IMU driver and to create one or more objects of the
                 IMU class which can be used to perform platform position
                 measurement.
                 
    '''
    def __init__(self):
        '''!@brief      Constructs an IMU object.
            @details    This method initializes variables of an object and I2C communication
                        between the IMU and the STM32.             
        '''
        ## Creates an I2C object for serial communications
        self.i2c = I2C(1, I2C.CONTROLLER)
        ## Defines BNO055 device address
        self.imu_addr = 0x28
        ## Defines register address for operating mode
        self.oper_mode = 0x3D
        ## Defines nDOF operating mode (12)
        self.ndof_mode = 0b1100
        ## Initializes variable for calibration status data
        self.cal_byte = 0
        ## Defines velocity data address
        self.vel_data_addr = 0x14
        ## Defines Euler angle data address
        self.euler_data_addr = 0x1A
        ## Defines calibration coefficient data address
        self.cal_coeff_addr = 0x55
        # Initialize in configuration mode
        self.i2c.mem_write(0, self.imu_addr, self.oper_mode)
        delay(19)
        self.i2c.mem_write(0x21, self.imu_addr, 0x41)
        self.i2c.mem_write(0x02, self.imu_addr, 0x42)
        
        ## Creates a Bytearray for managing Euler angles
        self.euler_buffer = bytearray(6*[0])
        ## Creates a Bytearray for managing angular velocities
        self.omega_buffer = bytearray(6*[0])
        

    def change_mode(self, mode):
        '''!@brief      Changes IMU mode.
            @details    This method accepts an operating mode according the BNO055 spec sheet
                        and sets the BNO055 into the respective mode.
            @param mode Provides the operating mode for the BNO055             
        '''
        self.i2c.mem_write(mode, self.imu_addr, self.oper_mode)
        delay(19)
        
    def get_cal_status(self):
        '''!@brief      Obtains the calibration status of the IMU.
            @details    This method retrieves the calibration states if the IMU and returns
                        the values for the magnetometer, acclerometer, gyroscope, and system.
                        A device with read a value of 3 for a succesful calibrated unit. Values
                        range from 0 to 3.        
        '''
        self.cal_byte = self.i2c.mem_read(1, self.imu_addr, 0x35)[0]
        ## Defines the magnetometer calibration status
        mag_stat = self.cal_byte & 0b00000011
        ## Defines the accelerometer calibration status
        acc_stat = (self.cal_byte & 0b00001100)>>2
        ## Defines the gyroscope calibration status
        gyr_stat = (self.cal_byte & 0b00110000)>>4
        ## Defines the system calibration status
        sys_stat = (self.cal_byte & 0b11000000)>>6
        return (mag_stat, acc_stat, gyr_stat, sys_stat)
    
    def get_cal_coeff(self, cal_buff):
        '''!@brief      Reads the IMU calibration coefficients.
            @details    Reads the IMU calibration coefficients from register of the BNO055.
            @param cal_buff Provides an intialized buffer to allocate the calibration constants.        
        '''
        self.i2c.mem_read(cal_buff, self.imu_addr, self.cal_coeff_addr)
        return cal_buff
    
    def set_cal_coeff(self, set_buff):
        '''!@brief      Writes the IMU calibration coefficients.
            @details    Writes the IMU calibration coefficients to the register of the BNO055.
            @param set_buff Provides a set buffer to write to the IMU register.       
        '''
        self.i2c.mem_write(set_buff, self.imu_addr, self.cal_coeff_addr)

    def get_euler(self):
        '''!@brief      Reads the IMU Euler angles.
            @details    Interacts with the IMU Euler angle register to return positional data. 
        '''
        self.i2c.mem_read(self.euler_buffer, self.imu_addr, self.euler_data_addr)
        ## Defines the head position
        head = (self.euler_buffer[1]<<8) | self.euler_buffer[0]
        ## Defines the roll position (x-theta)
        roll = (self.euler_buffer[3]<<8) | self.euler_buffer[2]
        ## Defines the pitch position (y-theta)
        pitch = (self.euler_buffer[5]<<8) | self.euler_buffer[4]
        
        # Overflow handling
        if head > 32767:
            head -= 65536
        if roll > 32767:
            roll -= 65536
        if pitch > 32767:
            pitch -= 65536

        return (-head/16, -roll/16, -pitch/16)
        
    def get_omega(self):
        '''!@brief      Reads the IMU angular velocities.
            @details    Interacts with the IMU angular velocity register to return velocity data.      
        '''
        self.i2c.mem_read(self.omega_buffer, self.imu_addr, self.vel_data_addr)
        gyr_x = (self.omega_buffer[1]<<8) | self.omega_buffer[0]
        gyr_y = (self.omega_buffer[3]<<8) | self.omega_buffer[2]
        gyr_z = (self.omega_buffer[5]<<8) | self.omega_buffer[4]
        
        # Overflow handling
        if gyr_x > 32767:
            gyr_x -= 65536
        if gyr_y > 32767:
            gyr_y -= 65536
        if gyr_z > 32767:
            gyr_z -= 65536

        return (gyr_x/16, gyr_y/16, gyr_z/16)

'''
if __name__ == "__main__":
    imu = BNO055()
    period = 10_000
    start_time = time.ticks_us()
    next_time = time.ticks_add(start_time, period)
    print(imu.get_euler())
    print(imu.get_omega())
    #print(imu.get_cal_status())

    file_name = "IMU_cal_coeffs.txt"
    
    S0_INIT = micropython.const(0)
    S1_RUN = micropython.const(1)
    S2_CALIBRATE = micropython.const(2)
    S3_SAVE_CALIBRATION = micropython.const(3)
    S4_WRITE_COEFFICIENTS = micropython.const(4)
    file_string = ''
    state = S0_INIT
    #print(imu.get_euler())
    #print(imu.get_omega())

    while True:
        current_time = time.ticks_us()
        if current_time >= next_time:
            
            if state == S0_INIT:

                if file_name in os.listdir():
                    #print('S0 -> S4')
                    state = S4_WRITE_COEFFICIENTS
                    
                else:
                    imu.change_mode(12)
                    #print('S0 -> S2')
                    state = S2_CALIBRATE
                

            elif state == S1_RUN:
                euler_angles = imu.get_euler()
                gyr_vels = imu.get_omega()
                #print(euler_angles)
                #print(gyr_vels)
                #print("Run!")
                break

            elif state == S2_CALIBRATE:
                cal_stat = imu.get_cal_status()
                mag = cal_stat[0]
                acc = cal_stat[1]
                gyr = cal_stat[2]
                sys = cal_stat[3]
                print(mag, acc, gyr, sys)
                if (mag, acc, gyr, sys) == (3, 3, 3, 3):
                    state = S3_SAVE_CALIBRATION

            elif state == S3_SAVE_CALIBRATION:
                
                buff = bytearray(22*[0])
                gc.collect()
                cal_data = imu.get_cal_coeff(buff)
                #print(cal_data)
                cal_coeff_list = []
                for coeff in cal_data:
                    cal_coeff_list.append(hex(coeff))
                #print("append")
                with open(file_name, 'w') as file:
                    file.write(','.join(cal_coeff_list))
                    
                    if len(file_string) == 22:
                        print(file_string)
                        file.write(file_string)
                        print('S3 -> S1')
                        state = S1_RUN
                    
                #print("written")


            elif state == S4_WRITE_COEFFICIENTS:
                with open(file_name, 'r') as file:
                    cal_data_string = file.readline()
                    cal_data = bytearray([int(cal_value) for cal_value in cal_data_string.strip().split(',')])
                    imu.change_mode(0)
                    imu.set_cal_coeff(cal_data)
                imu.change_mode(12)
                #print('S4 -> S1')
                state = S1_RUN
                
        next_time = time.ticks_add(start_time, period)
'''