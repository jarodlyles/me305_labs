'''!@file       task_user_0x05.py
    @brief      A module task for managing state transitions and user interface.
    @details    This file contains a user interface in the form of a task to interpret user 
                keyboard inputs and process conditionals to perform the actions 
                of the state transitions.
    @author     Jarod Lyles
    @date       February 17, 2022
'''


import pyb, time, array, micropython
from shares_0x05 import Share
import gc

## Defines constant for State 0
S0_INIT = micropython.const(0)
## Defines constant for State 1
S1_WAIT_FOR_CMD = micropython.const(1)
## Defines constant for State 2
S2_ACCEPT_DATA_INPUT = micropython.const(2)
## Defines constant for State 3
S3_PRINT_POSITION = micropython.const(3)
## Defines constant for State 4
S4_GET_VELOCITY = micropython.const(4)

## Initializes a serial port reader for user input
serport = pyb.USB_VCP()
        
def taskUser(taskName, period, eulerAngles, gyrVels, accAccel, setPoint, pFlag, dFlag, iFlag, lFlag, mFlag, MFlag, cFlag, vFlag, kFlag, wFlag, xFlag, yFlag, duty1, duty2, K_p, K_d, K_i, XsetPoint, YsetPoint):

    '''!@brief           Manages the user interfacing, state transitions, and
                         returning data to the user.
        @details         This task, taskUser(), accepts shared variables
                         for boolean flags and positional details to perform 
                         appropreiate state transitions and return values to
                         to the ueser interface.
        @param taskName  Assigns a name to the taskUser task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param position  Initializes a passed shared integer variable upon which encoder
                         position data can be assigned.
        @param eulerAngles  Initializes a passed shared variable for Euler angles.
        @param gyrVels   Initializes a passed shared variable for gyroscope velocities.
        @param K_p       Initializes a passed shared variable for proportional gain.
        @param K_d       Initializes a passed shared variable for derivative gain.
        @param XsetPoint Initializes a passed shared variable for x-theta set point.
        @param YsetPoint Initializes a passed shared variable for y-theta set point.
        @param pFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for proportional gain.
        @param dFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for derivative gain.
        @param lFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for Euler angles.
        @param vFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for angular velocities.
        @param xFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for x-theta set point.
        @param yFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for y-theta set point.
                         
    '''
    
    intro()
    
    ## Initializes an active state variable for transition management
    state = S0_INIT
    ## Initializes start time for the task encoder function
    start_time = time.ticks_us()
    ## Initializes time interval of update for the task encoder
    next_time = time.ticks_add(start_time, period)
    
    while True:
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()
        if time.ticks_diff(current_time,next_time) >= 0:
            next_time = time.ticks_add(next_time, period)
            
            if state == S0_INIT:
                state = S1_WAIT_FOR_CMD
                    
            elif state == S1_WAIT_FOR_CMD:
                if serport.any():
                    
                    ## Assigns a character variable for the read out from the serial coms
                    charIn = serport.read(1).decode()
                    
                    # The following if/else tree performs the conditional logic
                    # on the user input to determine state transtion from State 1 
                    # and properly flag for use in the task encoder.
                        
                    if charIn in {'l', 'L'}:
                        state = S3_PRINT_POSITION
                        lFlag.write(True)
                        print('Input L – Returning Current IMU Position')
                        print('_____________________________________________________________')

                    elif charIn in {'v', 'V'}:
                        state = S4_GET_VELOCITY
                        print('Input V - Get Gryroscope Velocity of IMU')
                        print('_____________________________________________________________')
                        vFlag.write(True)

                    elif charIn in {'x', 'X'}:
                        state = S2_ACCEPT_DATA_INPUT
                        xFlag.write(True)
                        buff = ''
                        print('Input X - Enter X-Theta Set Point for Closed-Loop Control')
                        print('          Set Point gain should be between -12 and 12')
                        print('_____________________________________________________________')

                    elif charIn in {'y', 'Y'}:
                        state = S2_ACCEPT_DATA_INPUT
                        yFlag.write(True)
                        buff = ''
                        print('Input Y - Enter Y-Theta Set Point for Closed-Loop Control')
                        print('          Set Point gain should be between -12 and 12')
                        print('_____________________________________________________________')

                    elif charIn in {'p', 'P'}:
                        state = S2_ACCEPT_DATA_INPUT
                        print('Input K - Enter Proportional Gain (K_p) for Closed-Loop Control')
                        print('          Proportional Control gain should be between 3 and 10')
                        print('_____________________________________________________________')
                        pFlag.write(True)
                        buff = ''
                    
                    elif charIn in {'d', 'D'}:
                        state = S2_ACCEPT_DATA_INPUT
                        print('Input D - Enter Derivative Gain (K_d) for Closed-Loop Control')
                        print('          Derivative Control gain should be between 0 and 2')
                        print('_____________________________________________________________')
                        dFlag.write(True)
                        buff = ''
                    
                    elif charIn in {'i', 'I'}:
                        state = S2_ACCEPT_DATA_INPUT
                        print('Input I - Enter Integral Gain (K_i) for Closed-Loop Control')
                        print('_____________________________________________________________')
                        iFlag.write(True)
                        buff = ''
                    
                    elif charIn in {'h', 'H'}:
                        intro()
                        state = S1_WAIT_FOR_CMD

                    else:
                        print(f'You entered {charIn}. This is not a valid command.')
            
            # The following if/else tree determines appropreiate actions per 
            # state assignment
            elif state == S2_ACCEPT_DATA_INPUT:
                if serport.any():
                    charIn = serport.read(1).decode()

                    if charIn.isdigit():
                        buff += charIn
                        print(charIn, end='' )
                        
                    elif charIn in {'-'}: 
                        if len(buff) == 0:
                            buff += charIn
                            print(charIn, end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}: 
                        if len(buff) > 0:
                            buff = buff[:-1]
                            print('')
                            print(buff)
                        elif len(buff) == 0:
                            pass
                            
                    elif charIn in {'.'}:
                        buff += charIn
                        print(charIn, end='')
                        
                    elif charIn in {'\n','\r'}:
                        print('\n')
                        if len(buff) == 0:
                            print('Invalid input. Please retry.')
                            state = S1_WAIT_FOR_CMD
                        if xFlag.read():
                            print('_____________________________________________________________')
                            if float(buff) < 12 and float(buff) > -12:
                                XsetPoint.write(float(buff))
                                setPoint.write((0, XsetPoint.read(), YsetPoint.read()))
                                print(f'Theta-X Setpoint set to {XsetPoint.read()}°')
                            else:
                                print('Invalid input. Please enter a setpoint between 12° and -12°.')
                            print('_____________________________________________________________')
                            xFlag.write(False)
                            state = S1_WAIT_FOR_CMD

                        if yFlag.read():
                            print('_____________________________________________________________')
                            if float(buff) < 12 and float(buff) > -12:
                                YsetPoint.write(float(buff))
                                setPoint.write((0, XsetPoint.read(), YsetPoint.read()))
                                print(f'Theta-Y Setpoint set to {YsetPoint.read()}°')
                                
                            else:
                                print('Invalid input. Please enter a setpoint between 12° and -12°.')
                            print('_____________________________________________________________')
                            yFlag.write(False)
                            state = S1_WAIT_FOR_CMD
                        
                        if pFlag.read():
                            print('_____________________________________________________________')
                            if float(buff) > 3 and float(buff) < 10:
                                K_p.write(float(buff))
                                print(f'Closed-loop proportional control gain set to {K_p.read()}')
                                
                            else:
                                print(f'\nInvalid input. Please enter a proportional control gain between 3 and 10. Preset is 5.')
                            print('_____________________________________________________________')
                            pFlag.write(False)
                            state = S1_WAIT_FOR_CMD

                        if dFlag.read():
                            print('_____________________________________________________________')
                            if float(buff) > 0 and float(buff) < 2:
                                K_d.write(float(buff))
                                print(f'Closed-loop derivative control gain set to {K_d.read()}')
                            else:
                                print(f'Invalid input. Please enter a derivative control gain between 0 and 2. Preset is 0.2.')
                            print('_____________________________________________________________')
                            dFlag.write(False)
                            state = S1_WAIT_FOR_CMD

                        if iFlag.read():
                            print('_____________________________________________________________')
                            if float(buff) > 0 and float(buff) < 2:
                                K_i.write(float(buff))
                                print(f'Closed-loop integral control gain set to {K_i.read()}')
                                
                            else:
                                print(f'Invalid input. Please enter a integral control gain between # and #. Preset is #.')
                            print('_____________________________________________________________')
                            iFlag.write(False)
                            state = S1_WAIT_FOR_CMD

                
            elif state == S3_PRINT_POSITION:
                if lFlag.read():
                    (head, pitch, roll) = eulerAngles.read()
                    print(f'Head:  {head}°')
                    print(f'Pitch: {pitch}°')
                    print(f'Roll:  {roll}°')
                    print('_____________________________________________________________')
                    lFlag.write(False)
                    state = S1_WAIT_FOR_CMD
            
            elif state == S4_GET_VELOCITY:
                if vFlag.read():
                    (x_vel, y_vel, z_vel) = gyrVels.read()
                    print('Angular Velocities')
                    print(f'About X-axis: {x_vel}°/s')
                    print(f'About Y-axis: {y_vel}°/s')
                    print(f'About Z-axis: {z_vel}°/s')
                    print('_____________________________________________________________')
                    vFlag.write(False)
                    state = S1_WAIT_FOR_CMD         
                    
            else:
                raise ValueError(f'Invalid state value in {taskName}: State {state} does not exist.')
                
            yield state
            
        else:
            yield None
    

# Function for printing the welcome / command window
def intro():
    print(' _____________________________________________________ ')
    print('|                                                     |')
    print('|              PLATFORM BALANCING INTERFACE           |')
    print('|_____________________________________________________|')
    print('| Available user commands:                            |')
    print('|  [L] or [l]   |   Print IMU position                |')
    print('|  [V] or [v]   |   Print IME velocity measurement    |')
    print('|  [P] or [p]   |   Set the proportional control gain |')
    print('|  [D] or [d]   |   Set the derivative control gain   |')
    print('|  [I] or [i]   |   Set the integral control gain     |')
    print('|  [X] or [x]   |   Set the theta-x setpoint          |')
    print('|  [Y] or [y]   |   Set the theta-y setpoint          |')
    print('|  [CTRL]+[C]   |   Interrupt program                 |')
    print('|_____________________________________________________|\n')
