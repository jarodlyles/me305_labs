'''!@file       task_motor_0x05.py
    @brief      A module for tasking the motor to enable the driver DRV8847.
    @details    This module initializes motor objects and tasks the motor to the appropreiate
            frequency of operation.          
    @author     Jarod Lyles
    @date       February 2, 2022
'''

import time
import closedLoop_0x05

closedLoopController = closedLoop_0x05.ClosedLoop()

def taskMotor(taskName, period, wFlag, motor, duty, K_p, K_d, K_i, eulerAngles, gyrVels, setPoint, motorNumber):
    '''!@brief           Manages the initialization and tasking of the motor drivers.
        @details         This task, taskMotor(), accepts shared variables
                         for boolean flags and duty cycle details to perform 
                         PWM drive on the motors.
        @param taskName  Assigns a name to the instance of the taskMotor task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param cFlag     Initializes a passed shared boolean flag for
                         clearing the motor from user input.
        @param duty      Assigns a value for a shared object containing the requested
                         PWM signal.
        @param motor     Passes a motor object to assigning duty cycle.
        @param K_p       Initializes a passed proportional gain constant.
        @param K_d       Initializes a passed derivative gain constant.
        @param eulerAngles Initializes a passed shared dataset for Euler angles.
        @param gyrVels     Initializes a passed shared dataset for angular velocities.
        @param setPoint    Initializes a passed shared variable for positional setpoint.
        @param motorNumber Defines the number of the motor in the system
    '''

    ## Initializes start time for the task encoder function
    start_time = time.ticks_us()
    ## Initializes first iteration time interval of update for the task encoder
    next_time = time.ticks_add(start_time, period)

    while True:
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()

        if time.ticks_diff(current_time, next_time) >= 0:
            # Set the duty cycle of the motors using shared variables
            '''
            if not wFlag.read():
                motor.set_duty(duty.read())
            '''
            closedLoopController.set_gain(K_p.read(), K_d.read(), K_i.read())
            closedLoopController.update(eulerAngles.read()[motorNumber], gyrVels.read()[motorNumber], setPoint.read()[motorNumber])
            duty.write(closedLoopController.L)
            motor.set_duty(duty.read())
            
            # Provides update time for the loop
            next_time = time.ticks_add(next_time, period)
                
        else:
            yield None
