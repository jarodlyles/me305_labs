'''!@file       main_0x05.py
    @brief      A testing program for balancing the inverted pendulum platform.
    @details    The main file executes the necessary tasks to carry out the program
                assigns necessary shared variables amongst source code files.
    @author     Jarod Lyles
    @date       March 5, 2022
'''

import shares_0x05, pyb
import task_user_0x05, task_motor_0x05, task_IMU_0x05, motor_0x05


## Assigns a shared boolean flag to manage velocity assignment
vFlag = shares_0x05.Share(False)
wFlag = shares_0x05.Share(False)
kFlag = shares_0x05.Share(False)
## Assigns a shared boolean flag to manage proportional gain assignment
pFlag = shares_0x05.Share(False)
## Assigns a shared boolean flag to manage derivative gain assignment
dFlag = shares_0x05.Share(False)
## Assigns a shared boolean flag to manage integral gain assignment
iFlag = shares_0x05.Share(False)
## Assigns a shared boolean flag to manage positional data return
lFlag = shares_0x05.Share(False)
mFlag = shares_0x05.Share(False)
MFlag = shares_0x05.Share(False)
cFlag = shares_0x05.Share(False)
## Assigns a shared boolean flag to manage velocity data return
vFlag = shares_0x05.Share(False)
wFlag = shares_0x05.Share(False)
## Assigns a shared boolean flag to manage x-theta set point assignment
xFlag = shares_0x05.Share(False)
## Assigns a shared boolean flag to manage y-theta set point assignment
yFlag = shares_0x05.Share(False)

## Assigns a shared variable for motor 1 duty cycle
duty1 = shares_0x05.Share(0)
## Assigns a shared variable for motor 2 duty cycle
duty2 = shares_0x05.Share(0)

## Assigns a shared variable for x-theta set point
XsetPoint = shares_0x05.Share(0)
## Assigns a shared variable for y-theta set point
YsetPoint = shares_0x05.Share(0)

setPoint = shares_0x05.Share((0, 0, 0))

## Assigns a shared tuple for Euler angles
eulerAngles = shares_0x05.Share((0, 0, 0))
## Assigns a shared tuple for angular velocities
gyrVels = shares_0x05.Share((0, 0, 0))
## Assigns a shared tuple for angular acceleration 
accAccel = shares_0x05.Share((0, 0, 0))
## Assigns a shared tuple for calibration status
calStat = shares_0x05.Share((0, 0, 0, 0))

## Assigns a shared variable for proportional gain
K_p = shares_0x05.Share(5)
## Assigns a shared variable for derivative gain
K_d = shares_0x05.Share(0.2)
## Assigns a shared variable for integral gain
K_i = shares_0x05.Share(0)

## Initializes pin B4 on the Nucleo for interface with the first motor
pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
## Initializes pin B5 on the Nucleo for interface with the first motor
pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
## Initializes pin B0 on the Nucleo for interface with the second motor
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
## Initializes pin B6 on the Nucleo for interface with the second motor
pinB1 = pyb.Pin(pyb.Pin.cpu.B1)

## Initializes Motor 1 object using the motor class object
motor_1 = motor_0x05.Motor(20_000, 1, pinB4, 2, pinB5)
## Initializes Motor 2 object using the motor class object
motor_2 = motor_0x05.Motor(20_000, 3, pinB0, 4, pinB1)

## Defines the period of operation of the task functions in microseconds
period = 10_000

if __name__ == '__main__':
    
    ## Initializes the user task
    task1 = task_user_0x05.taskUser('taskUser', period, eulerAngles, gyrVels, accAccel, setPoint, pFlag, dFlag, iFlag, lFlag, mFlag, MFlag, cFlag, vFlag, kFlag, wFlag, xFlag, yFlag, duty1, duty2, K_p, K_d, K_d, XsetPoint, YsetPoint)
    ## Initializes the motor 1 / controller 1 task
    task2 = task_motor_0x05.taskMotor('taskMotor1', period, wFlag, motor_1, duty1, K_p, K_d, K_i, eulerAngles, gyrVels, setPoint, 1)
    ## Initializes the motor 2 / controller 2 task
    task3 = task_motor_0x05.taskMotor('taskMotor2', period, wFlag, motor_2, duty2, K_p, K_d, K_i, eulerAngles, gyrVels, setPoint, 2)
    ## Initializes the IMU task
    task4 = task_IMU_0x05.taskIMU('taskIMU', period, eulerAngles, gyrVels, accAccel, calStat)
    
    ## Defines the task list to be run
    taskList = [task1, task2, task3, task4]
    
    while True:
        try:
            for task in taskList:
                next(task)
        
        except KeyboardInterrupt:
            break
        
        
print('PROGRAM TERMINATING')
    
