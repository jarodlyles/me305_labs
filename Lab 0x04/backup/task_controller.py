'''!@file       task_controller.py
    @brief      A module for tasking the controller.
    @details    This class...   
    @author     Jarod Lyles
    @date       February 2, 2022
'''

import pyb, time
import closedLoop

closedLoopController = closedLoop.ClosedLoop()

def taskController(taskName, period):
    '''!@brief           Manages the initialization and tasking of the motor drivers.
        @details         This task, taskMotor(), accepts shared variables
                         for boolean flags and duty cycle details to perform 
                         PWM drive on the motors.
        @param taskName  Assigns a name to the instance of the taskMotor task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param cFlag     Initializes a passed shared boolean flag for
                         clearing the motor from user input.
        @param duty1     Assigns a value for a shared object containing the requested
                         PWM signal for motor 1.
        @param duty2     Assigns a value for a shared object containing the requested
                         PWM signal for motor 2.
    '''
    
    ## Initializes start time for the task controller function
    start_time = time.ticks_us()
    ## Initializes first iteration time interval of update for the task controller
    next_time = time.ticks_add(start_time, period)

    while True:
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()

        if time.ticks_diff(current_time, next_time) >= 0:
            
            closedLoopController.set_gain(gain.read())
            closedLoopController.update(gain.read(), setPoint.read(), velocity.read())
            
            # Provides update time for the loop
            next_time = time.ticks_add(next_time, period)
                
        else:
            yield None
