'''!@file       DRV8847.py
    @brief      A driver for the DRV8847 from TI.
    @details    This class defines a DRV8847 object and a an associted motor object. 
    @author     Jarod Lyles
    @date       February 17, 2022
'''

from pyb import Pin, Timer, ExtInt
import time

class DRV8847:
    '''!@brief A motor driver class for the DRV8847 from TI.
        @details Objects of this class can be used to configure the DRV8847
        motor driver and to create one or moreobjects of the
        Motor class which can be used to perform motor
        control.
        
        Refer to the DRV8847 datasheet here:
        https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    def __init__(self):
        '''!@brief Initializes and returns a DRV8847 object.
        '''

        self.nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)
        self.nFAULT = Pin(Pin.cpu.B2, mode=Pin.OUT_PP)
        self.IRQ_src = ExtInt(self.nFAULT, mode=ExtInt.IRQ_FALLING, pull=Pin.PULL_NONE, callback=self.fault_cb)
        
        pass
        
    def enable(self):
        '''!@brief      Awakes the DRV8847 from sleep mode.
            @details    Disables the interrupt for 0.025ms, turns the motor on,
                        then re-enables interrupt to avoid initial fault trigger.
        '''
        self.IRQ_src.disable()
        self.nSLEEP.high()
        time.sleep_us(25)
        self.IRQ_src.enable()
        
        pass
        
    def disable(self):
        '''!@brief Sets the DRV8847 in sleep mode.
        '''
        self.nSLEEP.low()
        
        pass
    
    def fault_cb(self, IRQ_src):
        '''!@brief Callback function to run on fault condition.
            @param IRQ_src The source of the interrupt request.
        '''
        self.disable()
        print('MOTOR FAULT')
        
        pass
            
    def motor(self, freq, outA, IN_A_pin, outB, IN_B_pin):
        '''!@brief Creates a DC motor object connected to the DRV8847.
            @return An object of class Motor
        '''
        #print('MOTOR OBJECT CREATED')
        return Motor(freq, outA, IN_A_pin, outB, IN_B_pin)

class Motor: 
    '''!@brief     A motor class for one channel of the DRV8847.
        @details    Class can be used to apply PWM to a
                    DC motor.
    '''
    def __init__(self, freq, outA, IN_A_pin, outB, IN_B_pin): 
        '''!@brief          Initializes an object associated with a DC Motor.
            @details         Use the DRV8847 class to instantiate a motor object of this class.
            @param freq      Specifies frequency of the PWM timer.
            @param IN_A_pin  Specifies first pin to use with specific motor.
            @param IN_B_pin  Specifies second pin to use with specific motor.
            @param outA      Channel of the first pin used for motor control.
            @param outB      Channel of the second pin used for motor control.
        '''
        # Timer initialization
        self.PWM_tim = Timer(3, freq = freq) 
        
        # Pin initialization 
        self.IN_A = Pin(IN_A_pin, mode=Pin.OUT_PP) 
        self.IN_B = Pin(IN_B_pin, mode=Pin.OUT_PP) 
        # Channel initialization
        self.t3chA = self.PWM_tim.channel(outA, Timer.PWM_INVERTED, pin=self.IN_A) 
        self.t3chB = self.PWM_tim.channel(outB, Timer.PWM_INVERTED, pin=self.IN_B) 
         
        pass 
     
    def set_duty(self, duty):
        '''!@brief          Set the PWM duty cycle for the motor channel.
            @details        This method set the PWM duty cycle of the given motor. Direction of rotation is determined by sign on the input value.
            @param duty     A signed number of the duty cycle in the range of -100% to 100%                          
        '''

        # For a positive duty cycle
        if duty > 0: 
            # Drive channel A, channel B off
            self.t3chA.pulse_width_percent(abs(duty))
            self.t3chB.pulse_width_percent(0)

        # For a negative duty cycle
        elif duty < 0: 
            # Drive channel B, channel A off
            self.t3chA.pulse_width_percent(0)
            self.t3chB.pulse_width_percent(abs(duty))
        pass


if __name__ == '__main__':   
    nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP) 
    nSLEEP.high() 

    drv = DRV8847()
    motor_1 = drv.motor(20_000, 1, Pin.cpu.B4, 2, Pin.cpu.B5) 
    motor_1.set_duty(20) 
    motor_2 = drv.motor(20_000, 3, Pin.cpu.B0, 4, Pin.cpu.B1) 
    motor_2.set_duty(20)
