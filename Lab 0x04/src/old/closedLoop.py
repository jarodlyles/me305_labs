class ClosedLoop:

    def init(self):
        ## Initializes proportional gain constant
        self.K_p = 1
        ## Initializes max saturation limit of controller
        self.max = 100
        ## Initializes saturation limit of controller
        self.min = -100
        ## Initializes value for adjusted duty cycle
        self.L = 0

    def update(self, vel_ref, vel_meas):
        
        self.vel_ref = vel_ref
        self.vel_meas = vel_meas
        
        self.L = self.K_p * (vel_ref - vel_meas)
        '''
        if self.L > self.max:
            self.L = self.max
        elif self.L < self.min:
            self.L = self.min
        '''
        return self.L

    def set_gain(self, gain):
        
        self.K_p = gain
    
    def get_gain(self):

        return self.K_p

