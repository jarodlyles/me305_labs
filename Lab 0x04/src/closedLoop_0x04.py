'''!@file       closedLoop_0x04.py
    @brief      A driver for defining the closed loop object
    @details    This file consitutes a class that defines a closed loop motor calculation.
                This object accepts positional, velocity, and initial values to determine
                closed-loop motor control in the form of an adjusted duty cycle.
    @author     Jarod Lyles
    @date       March 2th, 2022
'''

class ClosedLoop:
    '''!@brief   A closed loop class for the calculation of adjusted duty cycle.
        @details A closed loop class for the calculation of adjusted duty cycle accepting
                 reference and measured velocities to correct the duty response.
                 
    '''

    def __init__(self):
        '''!@brief      Constructs a closed loop object.
            @details    This method initializes variables of an object, namely the adjusted duty cycle value and respective gain values.             
        '''
        ## Initializes proportional gain constant
        self.K_p = 1
        ## Initializes max saturation limit of controller
        self.max = 100
        ## Initializes saturation limit of controller
        self.min = -100
        ## Initializes value for adjusted duty cycle
        self.L = 0

    def update(self, vel_ref, vel_meas):
        '''!@brief      Performs update of the closed loop system.
            @details    This method performs update of the inner closed loop system, accepting
                        positional, velocity, and set point data to do calculation of adjusted duty cycle.             
        '''
        self.vel_ref = vel_ref
        self.vel_meas = vel_meas
        
        self.L = self.K_p * (vel_ref - vel_meas)
        '''
        if self.L > self.max:
            self.L = self.max
        elif self.L < self.min:
            self.L = self.min
        '''
        return self.L

    def set_gain(self, gain):
        '''!@brief      Sets the closed-loop gains of the system.
            @details    This method assigns new values to the proportional
                        gain control constants of the system.             
        '''
        self.K_p = gain
    
    def get_gain(self):
        '''!@brief      Returns the value of the closed-loop gain variables.
            @details    This method returns values of the proportional
                        gain control constants.             
        '''
        return self.K_p

