# -*- coding: utf-8 -*-
"""@file fibonacciAssignment_Lyles
This program, @file fibonacciAssignment_Lyles, accepts an index from the user 
in the form of a positive integer and returns the number correlating to the 
index in the Fibonacci sequence.
"""

# The fib() function accepts an index (positive integer), iterates over the 
# value of the index to develop the Fibonacci sequence, and returns the
# Fibonacci number at the specified index
def fib(idx):
    
    fibSequence = [0, 1] # Initializes a list with the initial values f1 & f0
    
    if idx == 0: # For an index of zero, f0 from the list is used
        fibNumber = fibSequence[idx]
    elif idx == 1: # For an index of one, f1 from the list is used
        fibNumber = fibSequence[idx]
    
    else: # For all other cases
        n = 1 # Initizalize the counting variable
        while n < idx: # Establish a while loop to run until the index value
            
            # Calculates the Fibonacci at position n
            f_n = fibSequence[n] + fibSequence[n-1]  
            
            # Appends the Fibonacci sequence to the initialized list
            fibSequence.append(f_n)
            
            # Updates the value of the counting variable
            n+=1
        
        # The indexed Fibonacci number is extract from the end list position
        fibNumber = fibSequence[-1]
        
    return fibNumber # The number is returned from the fib() function

# Ensures execution of the script as a standalone program in the interpreter
if __name__ =='__main__':
    
    # Defines a while loop to run the script continuously until broken
    while True:
        idx = 0 # Initializes the index value at zero for each iteration
        idx = input('Enter an index: ') # Accepts user input
        
        try: # Trys casting the input as a integer value
            idx = int(idx)
            
            # Raises an error in the case of a negative integer
            if idx < 0:
                error = ValueError('The input is not a positive integer!')
                raise error
                
        # Excepts for errors in casting input as an integer
        except:
            print('The input is not a positive integer!')
            continue
        
        # For correct inputs, the fib() function returns the indexed number
        else:
            print('Fibonacci number at '
                  'index {:} is {:}.'.format(idx, fib(idx)))
            
            # Allows the user a chance to exit the program or continue
            idx = input('Enter "q" to quit or press enter to continue: ')
            if idx == 'q' or idx == 'Q': 
                print('Goodbye!')
                break
            continue